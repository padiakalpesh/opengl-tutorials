//
//  main.c
//  OpenGL Highlights Tutorial
//
//  Created by Kalpesh Padia on 9/26/15.
//  Copyright (c) 2015 Kalpesh Padia. All rights reserved.
//

#include <stdio.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>

float eyeX = 5.0f;
float eyeY = 5.0f;
float eyeZ = 5.0f;

float upX = 0.0f;
float upY = 0.0f;
float upZ = 1.0f;

float lightPosX = 2.70f;
float lightPosY = 1.40f;
float lightPosZ = 2.80f;

int angle = 0;
int _refreshmilliseconds = (1 / 15.0) * 1000;

int edgeLength = 1050;
int disp = 0;
int lighting = 3;

int toggleRotate = 1;
int toggleWire = 0;
int toggleShader = 1;

float shininess = 8;
int polyCount = 15;

int vertexshader,fragmentshader;
int shaderProgram;

// Ambient light
GLfloat ambientCol[] = { 100/255.0f, 100/255.0f, 255/255.0f, 1.0f };

// Positioned light
GLfloat lightCol0[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightColBlack[] = {0, 0, 0, 1};

void timer(int value)
{
    glutPostRedisplay();
    
    if (toggleRotate) {
        angle += 2;
        angle %= 360;
    }
    
    glutTimerFunc(_refreshmilliseconds, timer, 0);
}

void draw_string(float x, float y, float z, char *txt)
{
    glColor3f(1,1,1);
    glRasterPos3f(x, y,z);
    while( *txt != '\0' ) {
        glutBitmapCharacter( GLUT_BITMAP_9_BY_15, *txt );
        txt++;
    }
}


void drawAxes()
{
    glBegin(GL_LINES);
    
    //X
    glColor4f(1,0,0,0.2);
    glVertex3f(0,0,0);
    glVertex3f(eyeX,0,0);
    
    //Y
    glColor4f(0,1,0,0.2);
    glVertex3f(0,0,0);
    glVertex3f(0,eyeY,0);
    
    //Z
    glColor4f(0,0,1,0);
    glVertex3f(0,0,0);
    glVertex3f(0,0,eyeZ);
    
    glEnd();
    
    draw_string(5.7, -0.1, 0, "+X");
    draw_string(0, 5.2, -0.2, "+Y");
    draw_string(0, -0.6, 5.2, "+Z");
}

void displayTitleString(float x, char *txt )
{
    glClearColor( 0, 0, 0, 1 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glMatrixMode( GL_MODELVIEW );		// Setup model transformations
    glLoadIdentity();
    gluLookAt(0, 0, 5, 0, 0, -5, 0, 1, 0);
    glPushMatrix();
    
    glColor3f(1,1,1);
    glRasterPos3f(x, 0, 0);
    while( *txt != '\0' ) {
        glutBitmapCharacter( GLUT_BITMAP_9_BY_15, *txt );
        txt++;
    }
    glPopMatrix();
    glFlush();
}

void display_obj(int useShader, int poly, int shiny)
{
    glClearColor(.33,.33,.33,1);
    //glClearColor(1,1,1,1);
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glMatrixMode( GL_MODELVIEW );		// Setup model transformations
    glLoadIdentity();
    gluLookAt( eyeX, eyeY, eyeZ, -eyeX, -eyeY, -eyeZ, upX, upY, upZ);
    glPushMatrix();
    
    drawAxes();
    
    // Use lighting
    GLfloat lightPos0[] = { lightPosX, lightPosY, lightPosZ, 1.0f };
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    
    //shading
    glShadeModel(GL_SMOOTH);
    
    //reset all
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColBlack);
    glLightfv(GL_LIGHT0, GL_AMBIENT, lightColBlack);
    glLightfv(GL_LIGHT0, GL_SPECULAR, lightColBlack);
    
    switch (lighting) {
        case 2:
            glLightfv(GL_LIGHT0, GL_SPECULAR, lightCol0);
            break;
        case 1:
            glLightfv(GL_LIGHT0, GL_DIFFUSE, ambientCol);
            break;
        case 0:
            glLightfv(GL_LIGHT0, GL_AMBIENT, ambientCol);
            break;
        default:
            glLightfv(GL_LIGHT0, GL_SPECULAR, lightCol0);
            glLightfv(GL_LIGHT0, GL_DIFFUSE, ambientCol);
            glLightfv(GL_LIGHT0, GL_AMBIENT, ambientCol);
            break;
    }
    
    //light position
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos0);
    
    // add material properties
    GLfloat materialPos[] = {ambientCol[0],ambientCol[1],ambientCol[2],0.0};

    //glMaterialfv(GL_FRONT, GL_AMBIENT, materialPos);
    glMaterialfv(GL_FRONT, GL_SPECULAR, lightCol0);
    glMaterialf(GL_FRONT, GL_SHININESS, shiny);
    
    if (useShader)
	    glUseProgram(shaderProgram);
    
    glPushMatrix();
    glRotatef(angle, 0.0f, 0.0f, 1.0f);
    
    if (toggleWire)
        glutWireSphere(3,poly, poly);
    else
        glutSolidSphere(3,poly, poly);
    glPopMatrix();
    
    glUseProgram(0);
    
    glDisable(GL_LIGHTING);
   
    //show where light is
    glPushMatrix();
    glTranslatef(lightPosX, lightPosY, lightPosZ);
    glutSolidSphere(0.1,100, 100);
    glPopMatrix();
    
    glFlush();
}

void display()
{
    glClearColor( 0, 0, 0, 1 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glEnable(GL_SCISSOR_TEST);
    char str[40];
    switch (disp) {
        case 2:
            //bottom right
            //all
            glViewport(.533*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            glScissor(.533*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            displayTitleString(-3.5, "Phong Shading - Proper");
            
            glViewport(.533*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            glScissor(.533*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            display_obj(1, 5, 8);
            
            sprintf(str, "Shininess = %0.2f", 8.0);
            draw_string(-3,0,7,str);
            sprintf(str, "%d Slices, %d Stacks", 5, 5);
            draw_string(18,-10,-20,str);

            //bottom left
            //only specular
            glViewport(0.066*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            glScissor(0.066*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            displayTitleString(-5.65, "Gouraud Shading - Missing Highlights");
            
            glViewport(0.066*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            glScissor(0.066*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            display_obj(0, 5, 8);
            
            sprintf(str, "Shininess = %0.2f", 8.0);
            draw_string(-3,0,7,str);
            sprintf(str, "%d Slices, %d Stacks", 5, 5);
            draw_string(18,-10,-20,str);
            
        case 1:
            //top right
            //only diffuse
            glViewport(.533*edgeLength, 0.934*edgeLength, .4*edgeLength, .066*edgeLength);
            glScissor(.533*edgeLength, 0.934*edgeLength, .4*edgeLength, .066*edgeLength);
            displayTitleString(-3.5, "Phong Shading - Proper");
            
            glViewport(.533*edgeLength, .534*edgeLength, .4*edgeLength, .4*edgeLength);
            glScissor(.533*edgeLength, .534*edgeLength, .4*edgeLength, .4*edgeLength);
            display_obj(1, polyCount, shininess);
            
            sprintf(str, "Shininess = %0.2f", shininess);
            draw_string(-3,0,7,str);
            sprintf(str, "%d Slices, %d Stacks", polyCount, polyCount);
            draw_string(18,-10,-20,str);
            

            //top left
            //No shading (only ambient light)
            glViewport(0.066*edgeLength, 0.934*edgeLength, .4*edgeLength, .066*edgeLength);
            glScissor(0.066*edgeLength, 0.934*edgeLength, .4*edgeLength, .066*edgeLength);
            displayTitleString(-5.65, "Gouraud Shading - Smeared Highlights");
            
            glViewport(0.066*edgeLength, .534*edgeLength, .4*edgeLength, .4*edgeLength);
            glScissor(0.066*edgeLength, .534*edgeLength, .4*edgeLength, .4*edgeLength);
            display_obj(0, polyCount, shininess);
            
            sprintf(str, "Light at (%0.2f, %0.2f, %0.2f)", lightPosX, lightPosY, lightPosZ);
            draw_string(3,-20,10.5,str);
            
            sprintf(str, "Shininess = %0.2f", shininess);
            draw_string(-3,0,7,str);
            sprintf(str, "%d Slices, %d Stacks", polyCount, polyCount);
            draw_string(18,-10,-20,str);
            
            break;
            
        default:
            break;
    }
    
    glDisable(GL_SCISSOR_TEST);
    glUseProgram(0);
    
    glutSwapBuffers();
}

void setupDisplay()
{
    glMatrixMode( GL_PROJECTION );	// Setup perspective projection
    glLoadIdentity();
    gluPerspective( 110, 1, 1, 40 );
    
    glMatrixMode( GL_MODELVIEW );		// Setup model transformations
    glLoadIdentity();
    gluLookAt( eyeX, eyeY, eyeZ, -eyeX, -eyeY, -eyeZ, upX, upY, upZ);
}

void keyboard(unsigned char c, int x, int y){
    
    switch (c) {
        case 'Z':
        case 'z':
            lightPosX += 0.05;
            break;
		case 'X':
        case 'x':
            lightPosX -= 0.05;
            break;

        case 'L':
        case 'l':
            lightPosX = 2.7f;
            lightPosY = 1.4f;
            lightPosZ = 2.8f;
            break;
        
        case '=':
            shininess += 0.1;
            if (shininess >=128)
                shininess = 128;
            break;
        case '-':
            shininess -= 0.1;
            if (shininess <=1)
                shininess = 1;
            break;
        case ']':
            polyCount++;
            break;
        case '[':
            polyCount = (polyCount == 2)? polyCount : polyCount - 1;
            break;
        case 'R':
        case 'r':
            toggleRotate = (++toggleRotate)%2;
            break;
        case ' ':
            disp = (++disp) % 3;
            //rtoggleShader = (++toggleShader)%2;
            break;
        case 13:
            toggleWire = (++toggleWire)%2;
            break;
        case 'A':
        case 'a':
            lighting = 0;
            break;
        case 'S':
        case 's':
            lighting = 2;
            break;
        case 'D':
        case 'd':
            lighting = 1;
            break;
        case 'F':
        case 'f':
            lighting = 3;
            break;
        default:
            break;
    }
    //glutPostRedisplay();
}

void keyboardSpecial(int key, int x, int y) {
    
    switch (key) {
        case GLUT_KEY_RIGHT:
            lightPosY += 0.05;
            break;
        case GLUT_KEY_LEFT:
            lightPosY -= 0.05;
            break;
        case GLUT_KEY_UP:
            lightPosZ += 0.05;
            break;
        case GLUT_KEY_DOWN:
            lightPosZ -= 0.05;
            break;
        default:
            break;
    }
    printf("%0.2f, %0.2f, %0.2f\n", lightPosX, lightPosY, lightPosZ);
    
    //glutPostRedisplay();
}


char *textFileRead(char *fn)
{
    FILE *fp;
    char *content = NULL;
    
    int count=0;
    
    if (fn != NULL) {
        fp = fopen(fn,"rt");
        
        if (fp != NULL) {
            
            fseek(fp, 0, SEEK_END);
            count = ftell(fp);
            rewind(fp);
            
            if (count > 0) {
                content = (char *)malloc(sizeof(char) * (count+1));
                count = fread(content,sizeof(char),count,fp);
                content[count] = '\0';
            }
            fclose(fp);
        }
    }
    return content;
}

void setupShaders()
{
    char *vs, *fs;
    
    // get a shader handler
    vertexshader = glCreateShader(GL_VERTEX_SHADER);
    // read the shader source from a file
    vs = textFileRead("vertexshader.txt");
    // conversions to fit the next function
    const char *vv = vs;
    // pass the source text to GL
    glShaderSource(vertexshader, 1, &vv,NULL);
    // free the memory from the source text
    free(vs);
    // finally compile the shader
    glCompileShader(vertexshader);
    
    // get a shader handler
    fragmentshader = glCreateShader(GL_FRAGMENT_SHADER);
    // read the shader source from a file
    fs = textFileRead("fragmentshader.txt");
    // conversions to fit the next function
    const char *fv = fs;
    // pass the source text to GL
    glShaderSource(fragmentshader, 1, &fv,NULL);
    // free the memory from the source text
    free(fs);
    // finally compile the shader
    glCompileShader(fragmentshader);
    
    shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram,vertexshader);
    glAttachShader(shaderProgram,fragmentshader);
    glLinkProgram(shaderProgram);
}

int main(int argc, char** argv)
{
    glutInit( &argc, argv );		// Initialize GLUT
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
    glutInitWindowSize( edgeLength, edgeLength );
    glutCreateWindow( "Highlight Demo" );
    
    setupDisplay();
    setupShaders();
    
    glClearDepth( 1.0 );			// Setup background colour
    glClearColor( 0.0, 0.0, 0.0, 1 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glEnable( GL_DEPTH_TEST );
    
    glutDisplayFunc(display);	// Setup GLUT callbacks
    glutTimerFunc(0, timer, 0);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(keyboardSpecial);
    
    glutMainLoop();
    
    return 0;
}