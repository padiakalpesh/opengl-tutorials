//
//  main.c
//  OpenGL Shading Tutorial
//
//  Created by Kalpesh Padia on 9/22/15.
//  Copyright (c) 2015 Kalpesh Padia. All rights reserved.
//

#include <stdio.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>

float eyeX = 5.0f;
float eyeY = 5.0f;
float eyeZ = 5.0f;

float upX = 0.0f;
float upY = 0.0f;
float upZ = 1.0f;

float lightPosX = 5.0f;
float lightPosY = 1.00f;
float lightPosZ = 3.00f;

int angle = 0;
int _refreshmilliseconds = (1 / 60.0) * 1000;

int toggleRotate = 0;

int model = 1;
int edgeLength = 1050;
int polyCount = 20;
int disp = 0;
float fps[] = {0, 0, 0, 0};
float mspf[] = {0, 0, 0, 0};

float mspfSum[] = {0, 0, 0, 0};

long frameCount[] = {0, 0, 0, 0};
long currentTime[] = {0, 0, 0, 0};
long previousTime[] = {0, 0, 0, 0};
int timeInterval[] = {0, 0, 0, 0};

float shininess = 8.0;

int vertexshader,fragmentshader;
int shaderProgram;

// Ambient light
GLfloat ambientCol[] = { 100/255.0f, 100/255.0f, 255/255.0f, 1.0f };

// Positioned light
GLfloat lightCol0[] = { 1.0f, 1.0f, 1.0f, 1.0f };

void timer(int value)
{
    glutPostRedisplay();
    
    if (toggleRotate) {
    	angle += 2;
    	angle %= 360;
    }
    
    glutTimerFunc(_refreshmilliseconds, timer, 0);
}

void draw_string(float x, float y, float z, char *txt)
{
    glColor3f(1,1,1);
    glRasterPos3f(x, y,z);
    while( *txt != '\0' ) {
        glutBitmapCharacter( GLUT_BITMAP_9_BY_15, *txt );
        txt++;
    }
}


void drawAxes()
{
    glBegin(GL_LINES);
    
    //X
    glColor4f(1,0,0,1);
    glVertex3f(0,0,0);
    glVertex3f(eyeX,0,0);
    
    //Y
    glColor4f(0,1,0,1);
    glVertex3f(0,0,0);
    glVertex3f(0,eyeY,0);
    
    //Z
    glColor4f(0,0,1,1);
    glVertex3f(0,0,0);
    glVertex3f(0,0,eyeZ);
    
    glEnd();
    
    draw_string(5.7, -0.1, 0, "+X");
    draw_string(0, 5.2, -0.2, "+Y");
    draw_string(0, -0.6, 5.2, "+Z");

}

void displayTitleString(float x, char *txt )
{
    glClearColor( 0, 0, 0, 1 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glMatrixMode( GL_MODELVIEW );		// Setup model transformations
    glLoadIdentity();
    gluLookAt(0, 0, 5, 0, 0, -5, 0, 1, 0);
    glPushMatrix();
    
    glColor3f(1,1,1);
    glRasterPos3f(x, 0, 0);
    while( *txt != '\0' ) {
        glutBitmapCharacter( GLUT_BITMAP_9_BY_15, *txt );
        txt++;
    }
    glPopMatrix();
    glFlush();
}

void display_obj(int angle, int shading, int poly, int useShader, int index)
{
    glClearColor(0.33,0.33,0.33,1);
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glMatrixMode( GL_MODELVIEW );		// Setup model transformations
    glLoadIdentity();
    gluLookAt( eyeX, eyeY, eyeZ, -eyeX, -eyeY, -eyeZ, upX, upY, upZ);
    glPushMatrix();
    
    drawAxes(index);

    // Increase frame count
    frameCount[index] = frameCount[index]+1;
    
    // add material properties
    GLfloat Material[] = { 1.0, 0.0, 0.0, 1.0 };
    //glMaterialfv(GL_FRONT, GL_AMBIENT, ambientCol);
    glMaterialfv(GL_FRONT, GL_SPECULAR, lightCol0);
    glMaterialf(GL_FRONT, GL_SHININESS, shininess);
    
    // Use lighting
    GLfloat lightPos0[] = { lightPosX, lightPosY, lightPosZ, 1.0f };
    
    //add ambient light to scene
    //glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientCol);
    
    //ambient, diffuse and specular components of light
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambientCol);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, ambientCol);
    glLightfv(GL_LIGHT0, GL_SPECULAR, lightCol0);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos0);
    
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glEnable(GL_NORMALIZE);
    
    if (useShader)
        glUseProgram(shaderProgram);
    else
    {
        //shading
        glShadeModel(shading);
    }
    
    // Get the number of milliseconds since glutInit called
    // (or first call to glutGet(GLUT ELAPSED TIME)).
    long startTime = glutGet(GLUT_ELAPSED_TIME);
    
    // Keyboard-controlled circle, player1
    glPushMatrix();
    glRotatef(angle, 0.0f, 0.0f, 1.0f);
    
    switch (model) {
        case 2: glutSolidCone(3.0f, 3.0f, poly, poly);
            break;
        case 3: glutSolidTorus(1.0f, 3.0f, poly, poly);
            break;
        case 4: glutSolidTeapot(3.0f);
            break;
        default:
            glutSolidSphere(4.0f, poly, poly);
            break;
    }
    
    glPopMatrix();
    glDisable(GL_LIGHTING);
    glUseProgram(0);
    
    //show where light is
    glPushMatrix();
    glTranslatef(lightPosX, lightPosY, lightPosZ);
    glutSolidSphere(0.1,100, 100);
    glPopMatrix();
    
    glFlush();
    
    currentTime[index] = glutGet(GLUT_ELAPSED_TIME);
    long endTime = currentTime[index];
    
    //  Calculate time passed
    long elapsedTime = endTime - startTime;
    
    mspfSum[index] += elapsedTime;
    
    
    if((currentTime[index] - previousTime[index]) > 5000)
    {
        //  calculate the number of frames per second
        fps[index] = frameCount[index] * 1000.0f / (currentTime[index] - previousTime[index]);
        mspf[index] = mspfSum[index] / frameCount[index];
        
        //  Set time
        previousTime[index] = currentTime[index];
        
        //  Reset frame count
        frameCount[index] = 0;
        mspfSum[index] = 0;
    }
}

void display()
{
    glClearColor( 0, 0, 0, 1 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glEnable(GL_SCISSOR_TEST);
    char str[30];
    switch (disp) {
        case 4:
            //bottom right
            //phong shading - low poly count
            glViewport(.533*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            glScissor(.533*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            displayTitleString(-4.75, "Phong Shading - Low Poly Count");
            
            glViewport(.533*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            glScissor(.533*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            display_obj(angle, GL_SMOOTH, polyCount, 1, 3);
            
            sprintf(str, "Shininess = %0.2f", shininess);
            draw_string(-3,0,7,str);
            sprintf(str, "%d Slices, %d Stacks", polyCount, polyCount);
            draw_string(18,-10,-20,str);
            sprintf(str, "FPS:%4.2f mSPF: %4.2f", fps[3], mspf[3]);
            draw_string(16,-10,-16,str);
            
            //sprintf(str, "Rank:");
            //draw_string(1.75,6,-7,str);
            
        case 3:
            //bottom left
            //gouraud shading - high poly count
            glViewport(0.066*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            glScissor(0.066*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            displayTitleString(-5, "Gouraud Shading - High Poly Count");
            
            glViewport(0.066*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            glScissor(0.066*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            display_obj(angle, GL_SMOOTH, 2*polyCount, 0, 2);
            
            sprintf(str, "Shininess = %0.2f", shininess);
            draw_string(-3,0,7,str);
            sprintf(str, "%d Slices, %d Stacks", 2*polyCount, 2*polyCount);
            draw_string(18,-10,-20,str);
            sprintf(str, "FPS:%4.2f mSPF: %4.2f", fps[2], mspf[2]);
            draw_string(16,-10,-16,str);
            
            //sprintf(str, "Rank: ");
            //draw_string(1.75,6,-7,str);
            
        case 2:
            //top right
            //gouraud shading - low poly count
            glViewport(.533*edgeLength, 0.934*edgeLength, .4*edgeLength, .066*edgeLength);
            glScissor(.533*edgeLength, 0.934*edgeLength, .4*edgeLength, .066*edgeLength);
            displayTitleString(-4.75, "Gouraud Shading - Low Poly Count");
            
            glViewport(.533*edgeLength, .534*edgeLength, .4*edgeLength, .4*edgeLength);
            glScissor(.533*edgeLength, .534*edgeLength, .4*edgeLength, .4*edgeLength);
            display_obj(angle, GL_SMOOTH, polyCount, 0, 1);
            
            sprintf(str, "Shininess = %0.2f", shininess);
            draw_string(-3,0,7,str);
            sprintf(str, "%d Slices, %d Stacks", polyCount, polyCount);
            draw_string(18,-10,-20,str);
            sprintf(str, "FPS:%4.2f mSPF: %4.2f", fps[1], mspf[1]);
            draw_string(16,-10,-16,str);
            
            //sprintf(str, "Rank: ");
            //draw_string(1.75,6,-7,str);
            
        case 1:
            //top left
            //flat shading
            glViewport(0.066*edgeLength, 0.934*edgeLength, .4*edgeLength, .066*edgeLength);
            glScissor(0.066*edgeLength, 0.934*edgeLength, .4*edgeLength, .066*edgeLength);
            displayTitleString(-2, "Flat Shading");
            
            glViewport(0.066*edgeLength, .534*edgeLength, .4*edgeLength, .4*edgeLength);
            glScissor(0.066*edgeLength, .534*edgeLength, .4*edgeLength, .4*edgeLength);
            display_obj(angle, GL_FLAT, polyCount, 0, 0);
            
            sprintf(str, "Light at (%0.2f, %0.2f, %0.2f)", lightPosX, lightPosY, lightPosZ);
            draw_string(3,-20,10.5,str);
            
            sprintf(str, "Shininess = %0.2f", shininess);
            draw_string(-3,0,7,str);
            sprintf(str, "%d Slices, %d Stacks", polyCount, polyCount);
            draw_string(18,-10,-20,str);
            sprintf(str, "FPS:%4.2f mSPF: %4.2f", fps[0], mspf[0]);
            draw_string(16,-10,-16,str);
            
            //sprintf(str, "Rank: ");
            //draw_string(1.75,6,-7,str);
            break;
            
        default:
            break;
    }
    
    glDisable(GL_SCISSOR_TEST);
    glUseProgram(0);
    
	glutSwapBuffers();
}

void setupDisplay()
{
    glMatrixMode( GL_PROJECTION );	// Setup perspective projection
    glLoadIdentity();
    gluPerspective( 110, 1, 1, 40 );
    
    glMatrixMode( GL_MODELVIEW );		// Setup model transformations
    glLoadIdentity();
    gluLookAt( eyeX, eyeY, eyeZ, -eyeX, -eyeY, -eyeZ, upX, upY, upZ);
}

void keyboard(unsigned char c, int x, int y){
    
    switch (c) {
        case 'Z':
        case 'z':
            lightPosX += 0.05;
            break;
		case 'X':
        case 'x':
            lightPosX -= 0.05;
            break;

        case 'L':
        case 'l':
            lightPosX = 5.0f;
            lightPosY = 1.0f;
            lightPosZ = 3.0f;
            break;
        case '=':
            shininess += 0.1;
            if (shininess >=128)
                shininess = 128;
            break;
        case '-':
            shininess -= 0.1;
            if (shininess <=1.0)
                shininess = 1.0;
            break;
        case ' ':
            disp =  (++disp) % 5;
            break;
        case ']':
            polyCount++;
            break;
        case '[':
            polyCount = (polyCount == 2)? polyCount : polyCount - 1;
            break;
        case 'R':
        case 'r':
            toggleRotate = (++toggleRotate)%2;
            break;
        default:
            break;
    }
    
    printf("%0.2f, %0.2f, %0.2f\n", lightPosX, lightPosY, lightPosZ);
}

void keyboardSpecial(int key, int x, int y) {
    
    switch (key) {
        case GLUT_KEY_RIGHT:
            lightPosY += 0.05;
            break;
        case GLUT_KEY_LEFT:
            lightPosY -= 0.05;
            break;
        case GLUT_KEY_UP:
            lightPosZ += 0.05;
            break;
        case GLUT_KEY_DOWN:
            lightPosZ -= 0.05;
            break;
        default:
            break;
    }
    printf("%0.2f, %0.2f, %0.2f\n", lightPosX, lightPosY, lightPosZ);
}

void menu(int value)
{
    model = value;
    printf("model = %d", model);
    // you would want to redraw now
    glutPostRedisplay();
}

void createMenu()
{
    glutCreateMenu(menu);
    glutAddMenuEntry("Sphere",1);
    glutAddMenuEntry("Cone",2);
    glutAddMenuEntry("Torus",3);
    glutAddMenuEntry("Teapot",4);
    glutAttachMenu(GLUT_RIGHT_BUTTON);
}

char *textFileRead(char *fn)
{
    FILE *fp;
    char *content = NULL;
    
    int count=0;
    
    if (fn != NULL) {
        fp = fopen(fn,"rt");
        
        if (fp != NULL) {
            
            fseek(fp, 0, SEEK_END);
            count = ftell(fp);
            rewind(fp);
            
            if (count > 0) {
                content = (char *)malloc(sizeof(char) * (count+1));
                count = fread(content,sizeof(char),count,fp);
                content[count] = '\0';
            }
            fclose(fp);
        }
    }
    return content;
}

void setupShaders()
{
    char *vs, *fs;
    
    // get a shader handler
    vertexshader = glCreateShader(GL_VERTEX_SHADER);
    // read the shader source from a file
    vs = textFileRead("vertexshader.txt");
    // conversions to fit the next function
    const char *vv = vs;
    // pass the source text to GL
    glShaderSource(vertexshader, 1, &vv,NULL);
    // free the memory from the source text
    free(vs);
    // finally compile the shader
    glCompileShader(vertexshader);
    
    // get a shader handler
    fragmentshader = glCreateShader(GL_FRAGMENT_SHADER);
    // read the shader source from a file
    fs = textFileRead("fragmentshader.txt");
    // conversions to fit the next function
    const char *fv = fs;
    // pass the source text to GL
    glShaderSource(fragmentshader, 1, &fv,NULL);
    // free the memory from the source text
    free(fs);
    // finally compile the shader
    glCompileShader(fragmentshader);
    
    shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram,vertexshader);
    glAttachShader(shaderProgram,fragmentshader);
    glLinkProgram(shaderProgram);
}

int main(int argc, char** argv)
{
    glutInit( &argc, argv );		// Initialize GLUT
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
    glutInitWindowSize( edgeLength, edgeLength );
    glutCreateWindow( "Shading Demo" );
    
    setupDisplay();
    setupShaders();
    createMenu();
    
    glClearDepth( 1.0 );			// Setup background colour
    glClearColor( 0.0, 0.0, 0.0, 1 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glEnable( GL_DEPTH_TEST );
    
    glutDisplayFunc(display);	// Setup GLUT callbacks
    glutTimerFunc(0, timer, 0);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(keyboardSpecial);
    
    glutMainLoop();
    
    return 0;
}