#OpenGL Tutorials
Basic OpenGL tutorials. First desgined to accompany classroom teaching of CSC 461. All tutorials use OpenGL immediate mode and designed to compile and run on Mac OSX.

##Blending
Basic tutorial to demonstrate how OpenGL performs blending, and why it is important to draw objects from back to front.

##Lighting
Basic tutorial to demonstrate how OpenGL performs lighting. Default lighting is performed using Gouraud Shading and Phong Lighting.  The tutorials compares the effects of ambient, diffuse, ambient+difuse, and ambient+diffuse+specular lighting.

##Shading
Basic tutorial to demonstrate differences between flat shading, Gouraud shading and Phong Shading. The first two are implemented in OpenGL immediate mode by calling glShade() with  GL_FLAT and GL_SMOOTH. Phong Shading is implemented using GLSL.

##Highlights
Basic tutorial to show the problem with specular highlights when using Gouraud shading. Compares specular highlights in Gouraud Shading with Phong Shading.

##Line Drawing
Basic tutorial to demonstrate how lines are drawn using the Bressenham's line drawing algorithm

##Color Model
Basic demonstration of additive vs. subtractive color models

##Hermite Cuves
Demonstrates how to create hermite curves

##Bezier Curves
Demonstrates how to draw Bézier Curves

##UNB-Splines
Demonstrates how to draw Uniform Nonrational B-Splines

##NNB-Splines
Demonstrates Non-uniform Nonrational B-Splines

##Texture Filtering Variations
Demonstrates the difference between nearest neighbor and bilinear interpolations.

##Toon Shading
Demonstrates how to perform toon shading using GLSL