//
//  main.cpp
//  Texture Filtering Variations
//
//  Created by Kalpesh Padia on 11/3/15.
//  Copyright (c) 2015 Kalpesh Padia. All rights reserved.
//

#include <iostream>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include <cmath>

int disp = 0;

int eyeX = 0;
int eyeY = 0;
int eyeZ = 5;

float delta = 0.01;
int edgeLength = 400;
int stacks = 4000;

//draws a circle of radius r, centered at origin using the midpoint circle algorithm
void Circle(float r)
{
    float x, y;
    float i = 0;
    
    for (i = 0; i < 360 ; i+=delta)
    {
        x = r * cos(M_PI * i /90.0);
        y = r * sin(M_PI * i /90.0);
        glVertex2f(x, y);
    }
    
}

void NN()
{
    float i = -1.0f;
    glBegin(GL_POINTS);
    
    while (i <= 1.001f)
    {
        float j = -1.0f;
        
        while (j <= 1.001f)
        {
            if (floor(i+1.0)<=0.0f && floor(j+1.0)<=0.0f)
            {
                glColor3f(1,0,0);
            }
            else if (floor(i+1.0)>0.0f && floor(j+1.0)<=0.0f)
            {
                glColor3f(0,1,0);
            }
            else if (floor(i+1.0)>0.0f && floor(j+1.0)>0.0f)
            {
                glColor3f(0,0,1);
            }
            else
            {
                glColor3f(1,1,1);
            }
            
            glVertex3f(i, j, 1);
            
            j += 0.01f;
        }
        i += 0.01f;
    }
    
    glEnd();
}

void bilinear()
{
    double i = -1.0f;
    
    double s;
    double t;
    glBegin(GL_POINTS);
    
    while (i <= 1.001f)
    {
        float j = -1.0f;
        
        while (j <= 1.001f)
        {
            
            t = (j+1.0)/2.0;
            s = (i+1.0)/2.0;
            
            glColor3f(1-s, s + t - 2*t*s, t);
            
            glVertex3f(i, j, 1);
            
            j += 0.01f;
        }
        i += 0.01f;
    }
    
    glEnd();
}

void displayTitleString(float x, char *txt )
{
    glMatrixMode( GL_MODELVIEW );		// Setup model transformations
    glLoadIdentity();
    gluLookAt(0, 0, 5, 0, 0, -5, 0, 1, 0);
    glPushMatrix();
    
    glColor3f(1,1,1);
    glRasterPos3f(x, 0, 1);
    while( *txt != '\0' ) {
        glutBitmapCharacter( GLUT_BITMAP_9_BY_15, *txt );
        txt++;
    }
    glPopMatrix();
    glFlush();
}


void display(int colorModel)
{
    glMatrixMode( GL_MODELVIEW );		// Setup model transformations
    glLoadIdentity();
    gluLookAt( eyeX, eyeY, eyeZ, -eyeX, -eyeY, -eyeZ, 0, 1, 0 );
    glPushMatrix();			// Save glLookAt transformation
    
    switch (colorModel)
    {
        case 1:
            glClearColor( 0, 0, 0, 1 );
            glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
            
            NN();
            break;
            
        case 2:
            glClearColor( 0, 0, 0, 1 );
            glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
            
            bilinear();
            break;
        default:
            glClearColor( 0, 0, 0, 1 );
            glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
            
            glPointSize(5.0);
            glBegin(GL_POINTS);
            glColor3f(1,0,0);
            glVertex3f(-1,-1,1);
            
            glColor3f(0,1,0);
            glVertex3f(1,-1,1);
            
            glColor3f(0,0,1);
            glVertex3f(1,1,1);
            
            glColor3f(1,1,1);
            glVertex3f(-1,1,1);
            glEnd();
    }
    
    glFlush();
}

void display_obj()
{
    glClearColor( 0.3, 0.3, 0.3, 1 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glEnable(GL_SCISSOR_TEST);
    switch (disp) {
        case 2:
            //bottom right
            //phong shading - low poly count
            glViewport(.533*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            glScissor(.533*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            displayTitleString(-0.25, "BILINEAR");
            
            glViewport(.533*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            glScissor(.533*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            display(2);
            
        case 1:
            //bottom left
            //gouraud shading - high poly count
            glViewport(0.066*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            glScissor(0.066*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            displayTitleString(-0.5, "NEAREST NEIGHBOR");
            
            glViewport(0.066*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            glScissor(0.066*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            display(1);
            
            
        default:
            
            glViewport(0.066*edgeLength, 0.934*edgeLength, .868*edgeLength, .066*edgeLength);
            glScissor(0.066*edgeLength, 0.934*edgeLength, .868*edgeLength, .066*edgeLength);
            displayTitleString(-0.1, "TEXTURE");
            
            glViewport(0.3*edgeLength, 0.534*edgeLength, .4*edgeLength, .4*edgeLength);
            glScissor(0.3*edgeLength, 0.534*edgeLength, .4*edgeLength, .4*edgeLength);
            display(0);
            
            break;
    }
    
    glDisable(GL_SCISSOR_TEST);
    
    glutSwapBuffers();
}



void keyboard(unsigned char c, int x, int y){
    if (c == ' ')
    {
        disp = ++disp % 3;
    }
    
    if (c == 27)
    {
        disp = --disp % 3;
        
        if (disp < 0)
        {
            disp = 0;
        }
    }
    
    if (c == 'Z' || c == 'z')
    {
        eyeZ++;
    }
    
    if (c == 'x' || c == 'X')
    {
        eyeZ--;
    }
    
    display_obj();
}

void keyboardSpecial(int key, int x, int y) {
    
    switch (key) {
        case GLUT_KEY_RIGHT:
            eyeX++;
            break;
        case GLUT_KEY_LEFT:
            eyeX--;
            break;
        case GLUT_KEY_UP:
            eyeY++;
            break;
        case GLUT_KEY_DOWN:
            eyeY--;
            break;
            
        default:
            break;
    }
    
    display_obj();
}

int main(int argc, char** argv)
{
    glutInit( &argc, argv );		// Initialize GLUT
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
    glutInitWindowSize( edgeLength, edgeLength );
    glutCreateWindow( "Color Demo" );
    
    glutDisplayFunc( display_obj );	// Setup GLUT callbacks
    
    glMatrixMode( GL_PROJECTION );	// Setup perspective projection
    glLoadIdentity();
    //gluPerspective( 110, 1, 1, 40 );
    glOrtho(-1.1,1.1,-1.1,1.1,4,-4);
    
    glMatrixMode( GL_MODELVIEW );		// Setup model transformations
    glLoadIdentity();
    //gluLookAt( 5, 5, 5, -5, -5, -5, 0, 1, 0 );
    gluLookAt( eyeX, eyeY, eyeZ, -eyeX, -eyeY, -eyeZ, 0, 1, 0 );
    
    glShadeModel( GL_SMOOTH );
    
    glClearDepth( 1.0 );			// Setup background colour
    glClearColor( 0, 0, 0, 1 );
    
    //glDepthFunc(GL_LEQUAL);
    //glEnable( GL_DEPTH_TEST );
    
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(keyboardSpecial);
    
    glutMainLoop();
    
    return 0;
}