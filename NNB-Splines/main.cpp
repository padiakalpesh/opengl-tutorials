//
//  main.cpp
//  NNB-Splines
//
//  Created by Kalpesh Padia on 10/26/15.
//  Copyright © 2015 Kalpesh Padia. All rights reserved.
//

#include <cstdio>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include <cmath>
#include <string>

#define MAX_T 20

using namespace std;

int left = -20;
int right = 20;
int top = 20;
int bottom  = -20;

float edgeLength = 600;

//vector < vector<float> > points;
//vector <int> ti;
float pointsA[][2] = {
    {-7, 12},
    {-7, 7},
    {-15, 7},
    {-15, 17},
    {-5, 17},
    {-5, 7},
    {5, 7},
    {5, 17},
    {-3, 17},
    {-3, 12}
};


float pointsB[][2] = {
    {-7, 0},
    {-7, -5},
    {-15, -5},
    {-15, 5},
    {-5, 5},
    {-5, -5},
    {5, -5},
    {5, 5},
    {-3, 5},
    {-3, 0}
};

float pointsC[][2] = {
    {-7, -12},
    {-7, -17},
    {-15, -17},
    {-15, -7},
    {-5, -7},
    {-5, -17},
    {5, -17},
    {5, -7},
    {-3, -7},
    {-3, -12}
};


int tiA[] = {
    0, 1, 2, 			//first (to pin)
    3, 4, 5, 6, 7, 8, 9, 10, //knot points
    11, 12, 13 			//last (to pin)
};



int tiB[] = {
    0, 1, 2, 			//first (to pin)
    3, 4, 5, 5, 6, 7, 8, 9, //knot points
    10, 11, 12 			//last (to pin)
};

 
int tiC[] = {
    0, 1, 2, 			//first (to pin)
    3, 4, 5, 5, 5, 6, 7, 8, //knot points
    9, 10, 11 			//last (to pin)
};

int *ti;

int timeSteps = 0;
int jLimit = 0;

int points_size()
{
    return 10;
}

int ti_size()
{
    return points_size() + 4;
}

float Bi_t(int i, int j, float t)
{
    //printf("(%d, %d, %f) -> ", i, j, t);
    if (i >= ti_size())
        return 1;
    
    if (j == 1)
    {
        if (ti[i] <= t && t < ti[i+1])
        {
            return 1.0f;
        }
        else
        {
            return 0.0f;
        }
    }
    else
    {
        float a, b;
        if (ti[i+j-1] == ti[i])
            a = 0;
        else
            a = (t-ti[i])/(ti[i+j-1]-ti[i]);
        
        if (ti[i+j] == ti[i+1])
            b = 0;
        else
            b = (ti[i+j]-t)/(ti[i+j]-ti[i+1]);
        
        float res = (a * Bi_t(i, j-1, t)) + (b * Bi_t(i+1, j-1, t));
        //printf("%f\n", res);
        return res;
    }
}
void draw_string(float x, float y, float z, const char *txt)
{
    glColor3f(0,0,0);
    glRasterPos3f(x, y,z);
    while( *txt != '\0' ) {
        glutBitmapCharacter( GLUT_BITMAP_9_BY_15, *txt );
        txt++;
    }
}

//void drawSegment(vector<float> p1, vector<float> r1, vector<float> r4, vector<float> p4, int i)
void drawSegment(float* p1, float* r1, float* r4, float* p4, int i)
{
    float x, y, z;
    float delta = (ti[i+1] - ti[i])*1.0/MAX_T;
    float t;
    int ts;
    
    glLineWidth(3.0);
    glBegin(GL_LINE_STRIP);
    
    ts = MAX_T;
    
    //using float causes error in precision
    for (int j=0; j<= ts; j++)
    {
        t = delta * j;
        
        glColor3f(0,0,0);
        x = (p1[0] * Bi_t(i-3, 4, ti[i] + t)) + (r1[0] * Bi_t(i-2, 4, ti[i] + t)) + (r4[0] * Bi_t(i-1, 4, ti[i] + t)) + (p4[0] * Bi_t(i, 4, ti[i] + t));
        y = (p1[1] * Bi_t(i-3, 4, ti[i] + t)) + (r1[1] * Bi_t(i-2, 4, ti[i] + t)) + (r4[1] * Bi_t(i-1, 4, ti[i] + t)) + (p4[1] * Bi_t(i, 4, ti[i] + t));
        
        if (x == 0 && y == 0)
        {
            //x = p4[0];
            //y = p4[1];
        }
        
        z = 3.0;
        
        glVertex3f(x, y, z);
    }
    
    glEnd();
    
    //printf("%d %f %f %f %f\n", i, Bi_t(i-3, 4, ti[i]), Bi_t(i-2, 4, ti[i]), Bi_t(i-2, 4, ti[i]), Bi_t(i-2, 4, ti[i]));
    
    //knot points
    glPointSize(6.0);
    glColor3f(1,0.3,0.7);
    glBegin(GL_POINTS);
    
    t = 0;
    x = (p1[0] * Bi_t(i-3, 4, ti[i] + t)) + (r1[0] * Bi_t(i-2, 4, ti[i] + t)) + (r4[0] * Bi_t(i-1, 4, ti[i] + t)) + (p4[0] * Bi_t(i, 4, ti[i] + t));
    y = (p1[1] * Bi_t(i-3, 4, ti[i] + t)) + (r1[1] * Bi_t(i-2, 4, ti[i] + t)) + (r4[1] * Bi_t(i-1, 4, ti[i] + t)) + (p4[1] * Bi_t(i, 4, ti[i] + t));
    if (x == 0 && y == 0)
    {
        //x = p4[0];
        //y = p4[1];
    }
    z = 3.0;
    glVertex3f(x, y, z);
    
    t = (ti[i+1] - ti[i]);
    x = (p1[0] * Bi_t(i-3, 4, ti[i] + t)) + (r1[0] * Bi_t(i-2, 4, ti[i] + t)) + (r4[0] * Bi_t(i-1, 4, ti[i] + t)) + (p4[0] * Bi_t(i, 4, ti[i] + t));
    y = (p1[1] * Bi_t(i-3, 4, ti[i] + t)) + (r1[1] * Bi_t(i-2, 4, ti[i] + t)) + (r4[1] * Bi_t(i-1, 4, ti[i] + t)) + (p4[1] * Bi_t(i, 4, ti[i] + t));
    if (x == 0 && y == 0)
    {
        //x = p4[0];
        //y = p4[1];
    }
	z = 3.0;
    glVertex3f(x, y, z);
    glEnd();
    
}

void drawGrid()
{
    glEnable(GL_LINE_SMOOTH);
    
    glColor4f(.9,.9,.9,1);
    glLineWidth(1.0);
    
    glBegin(GL_LINES);
    for (int i=left+1; i<right-1; i++)
    {
        if (i == left+1)
            continue;
        glVertex3f(i, bottom+1, 0);
        glVertex3f(i, top-1, 0);
    }
    
    for (int i=bottom+1; i<top-1; i++)
    {
        if (i == bottom+1)
            continue;
        glVertex3f(left+1, i, 0);
        glVertex3f(right-1, i, 0);
    }
    
    glColor4f(.8,.8,.8,1);
    glLineWidth(2);
    glVertex3f(left+1, 0, 0);
    glVertex3f(right-1, 0, 0);
    
    glVertex3f(0, bottom+1, 0);
    glVertex3f(0, top-1, 0);
    
    
    glLineWidth(5.0);
    
    glColor4f(0,1,0,1);
    glVertex3f(left+1, bottom+1, 0);
    glVertex3f(right-1, bottom+1, 0);
    
    glVertex3f(right-1, bottom+1, 0);
    glVertex3f(right-1, top-1, 0);
    
    glVertex3f(right-1, top-1, 0);
    glVertex3f(left+1, top-1, 0);
    
    glVertex3f(left+1, top-1, 0);
    glVertex3f(left+1, bottom+1, 0);
    
    glEnd();
}

void display()
{
    glClearColor( 0.95, 0.95, 0.95, 1 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    drawGrid();
    string str = "{";
    
    glPointSize(8.0);
    glColor3f(0,0,0);
    glBegin(GL_POINTS);
    for (int i=0; i<points_size(); i++)
    {
        glVertex3f(pointsA[i][0], pointsA[i][1], 3.0f);
    }
    glEnd();
    
    glLineWidth(1.0);
    glColor3f(.5,0,1);
    glBegin(GL_LINE_STRIP);
    for (int i=0; i<points_size(); i++)
    {
        glVertex3f(pointsA[i][0], pointsA[i][1], 3.0f);
    }
    glEnd();
    
    for (int i = 3; i < points_size(); i++)
    {
        ti = tiA;
        drawSegment(pointsA[i-3], pointsA[i-2], pointsA[i-1], pointsA[i], i);
    }
    
    for (int i=0; i <= ti_size()/2; i++)
    {
        str += (to_string(ti[i]) + ", ");
    }
    draw_string (5.5, 13, 3, str.c_str());
    str = "";
    for (int i=ti_size()/2 + 1; i < ti_size()-1; i++)
    {
        str += (to_string(ti[i]) + ", ");
    }
    str += (to_string(ti[ti_size()-1]) + "}");
    draw_string (6, 11, 3, str.c_str());
    
    
    
    
    glPointSize(8.0);
    glColor3f(0,0,0);
    glBegin(GL_POINTS);
    for (int i=0; i<points_size(); i++)
    {
        glVertex3f(pointsB[i][0], pointsB[i][1], 3.0f);
    }
    glEnd();
    
    glLineWidth(1.0);
    glColor3f(.5,0,1);
	glBegin(GL_LINE_STRIP);
    for (int i=0; i<points_size(); i++)
    {
        glVertex3f(pointsB[i][0], pointsB[i][1], 3.0f);
    }
    glEnd();
    
    for (int i = 3; i < points_size(); i++)
    {
        ti = tiB;
        drawSegment(pointsB[i-3], pointsB[i-2], pointsB[i-1], pointsB[i], i);
    }
    
    str = "{";
    for (int i=0; i <= ti_size()/2; i++)
    {
        str += (to_string(ti[i]) + ", ");
    }
    draw_string (5.5, 1, 3, str.c_str());
    str = "";
    for (int i=ti_size()/2 + 1; i < ti_size()-1; i++)
    {
        str += (to_string(ti[i]) + ", ");
    }
    str += (to_string(ti[ti_size()-1]) + "}");
    draw_string (6, -1, 3, str.c_str());
    
    
    
    glPointSize(8.0);
    glColor3f(0,0,0);
    glBegin(GL_POINTS);
    for (int i=0; i<points_size(); i++)
    {
        glVertex3f(pointsC[i][0], pointsC[i][1], 3.0f);
    }
    glEnd();
    
    glLineWidth(1.0);
    glColor3f(.5,0,1);
	glBegin(GL_LINE_STRIP);
    for (int i=0; i<points_size(); i++)
    {
        glVertex3f(pointsC[i][0], pointsC[i][1], 3.0f);
    }
    glEnd();
    
    for (int i = 3; i < points_size(); i++)
    {
        ti = tiC;
        drawSegment(pointsC[i-3], pointsC[i-2], pointsC[i-1], pointsC[i], i);
    }
    
    str = "{";
    for (int i=0; i <= ti_size()/2; i++)
    {
        str += (to_string(ti[i]) + ", ");
    }
    draw_string (5.5, -11, 3, str.c_str());
    str = "";
    for (int i=ti_size()/2 + 1; i < ti_size()-1; i++)
    {
        str += (to_string(ti[i]) + ", ");
    }
    str += (to_string(ti[ti_size()-1]) + "}");
    draw_string (6, -13, 3, str.c_str());
    
    glFlush();
    glutSwapBuffers();
}

/*
void keyboard(unsigned char key, int x, int y)
{
    if (key == 27)
    {
        printf("Clear");
        points.clear();
        glutPostRedisplay();
    }
}

void mouse(int button, int state, int x, int y)
{
    int viewport[4] = {0, 0, edgeLength, edgeLength};
    double modelview[16];
    double projection[16];
    
    float winX, winY, winZ;
    double posX, posY, posZ;
    
    vector<float> pos;
    
    glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
    glGetDoublev( GL_PROJECTION_MATRIX, projection );
    //glGetIntegerv( GL_VIEWPORT, viewport );
    
    if (button == GLUT_LEFT_BUTTON && state == GLUT_UP)
    {
        
        gluUnProject( (float)(x), (float)(600-y), 0.5, modelview, projection, viewport, &posX, &posY, &posZ);
        printf("%d %d, %f %f %f \n", x, y, posX, posY, posZ);
        
        if (posX <= left+1 || posX >= right-1 || posY <= bottom+1 || posY >= top-1)
            return;
        
        pos.push_back(posX);
        pos.push_back(posY);
        pos.push_back(3.0);
        
        points.push_back(pos);
        
        glutPostRedisplay();
    }
}
*/

int main(int argc, char** argv)
{
    glutInit( &argc, argv );		// Initialize GLUT
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
    glutInitWindowSize( edgeLength, edgeLength );
    glutCreateWindow( "NNB-Splines Demo" );
    
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    glOrtho(left, right, bottom, top, 3, -3);
    
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();
    
    glutDisplayFunc( display );	// Setup GLUT callbacks
    //glutKeyboardFunc(keyboard);
    //glutMouseFunc(mouse);
    
    
    glutMainLoop();
    
    return 0;
}
