//
//  main.c
//  OpenGL Blend Tutorial
//
//  Created by Kalpesh Padia on 9/20/15.
//  Copyright (c) 2015 Kalpesh Padia. All rights reserved.
//

#include <stdio.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>

int draw = 0;

int eyeX = 0;
int eyeY = 0;
int eyeZ = 5;

void drawAxes()
{
    glBegin(GL_LINES);
    
    glColor4f(1,1,0,1);
    glVertex3f(0,0,0);
    glVertex3f(5,0,0);
    
    glColor4f(0,1,1,1);
    glVertex3f(0,0,0);
    glVertex3f(0,5,0);
    
    glColor4f(1,0,1,1);
    glVertex3f(0,0,0);
    glVertex3f(0,0,5);
    
    glEnd();
}

void drawRed()
{
    //red
    glColor4f( 1, 0, 0, 1);
    glBegin(GL_QUADS);
    glVertex3f(-1.5, -1.5, 0.5);
    glVertex3f(1.5, -1.5, 0.5);
    glVertex3f(1.5, 1.5, 0.5);
    glVertex3f(-1.5, 1.5, 0.5);
    glEnd();
}

void drawGreen()
{
    //green
    glColor4f( 0, 1, 0, 0.5);
    glBegin(GL_QUADS);
    glVertex3f(-1, -1, 1);
    glVertex3f(1, -1, 1);
    glVertex3f(1, 1, 1);
    glVertex3f(-1, 1, 1);
    glEnd();
}

void drawBlue()
{
    //blue
    glColor4f( 0, 0, 1, 0.5);
    glBegin(GL_QUADS);
    glVertex3f(-0.5, -0.5, 1.5);
    glVertex3f(0.5, -0.5, 1.5);
    glVertex3f(0.5, 0.5, 1.5);
    glVertex3f(-0.5, 0.5, 1.5);
    glEnd();
}

void drawSquares(draw)
{
    switch (draw) {
        case 1:
            drawRed();
            break;
            
        case 2:
            drawRed();
            drawGreen();
            break;
        
        case 3:
            drawRed();
            drawGreen();
            drawBlue();
            break;
        default:
            break;
    }
}

void drawSquaresReversed()
{
    switch (draw) {
        case 1:
            drawBlue();
            break;
            
        case 2:
            drawBlue();
            drawGreen();
            break;
            
        case 3:
            drawBlue();
            drawGreen();
            drawRed();
            break;
        
        default:
            break;
    }

}

void display_obj()
{
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glMatrixMode( GL_MODELVIEW );		// Setup model transformations
    glLoadIdentity();
    gluLookAt( eyeX, eyeY, eyeZ, -eyeX, -eyeY, -eyeZ, 0, 1, 0 );
    glPushMatrix();			// Save glLookAt transformation
    
    drawAxes();
    glEnable(GL_BLEND);
    
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    
    drawSquares(draw);
    //drawSquaresReversed(draw);
    
    glDisable(GL_BLEND);
    
    
    glFlush();
    glutSwapBuffers();
}

void keyboard(unsigned char c, int x, int y){
    if (c == ' ')
    {
        draw = ++draw % 4;
    }
    
    if (c == 27)
    {
        draw = --draw % 4;
        
        if (draw < 0)
        {
            draw = 0;
        }
    }
    
    if (c == 'Z' || c == 'z')
    {
        eyeZ++;
    }
    
    if (c == 'x' || c == 'X')
    {
        eyeZ--;
    }
    
    display_obj();
}

void keyboardSpecial(int key, int x, int y) {
    
    switch (key) {
        case GLUT_KEY_RIGHT:
            eyeX++;
            break;
        case GLUT_KEY_LEFT:
            eyeX--;
            break;
        case GLUT_KEY_UP:
            eyeY++;
            break;
        case GLUT_KEY_DOWN:
            eyeY--;
            break;
            
        default:
            break;
    }
    
    display_obj();
}

int main(int argc, char** argv)
{
    glutInit( &argc, argv );		// Initialize GLUT
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
    glutInitWindowSize( 800, 800 );
    glutCreateWindow( "Blending Demo" );
    
    glutDisplayFunc( display_obj );	// Setup GLUT callbacks
    
    glMatrixMode( GL_PROJECTION );	// Setup perspective projection
    glLoadIdentity();
    gluPerspective( 110, 1, 1, 40 );
    
    glMatrixMode( GL_MODELVIEW );		// Setup model transformations
    glLoadIdentity();
    //gluLookAt( 5, 5, 5, -5, -5, -5, 0, 1, 0 );
    gluLookAt( eyeX, eyeY, eyeZ, -eyeX, -eyeY, -eyeZ, 0, 1, 0 );
    
    glShadeModel( GL_SMOOTH );
    
    glClearDepth( 1.0 );			// Setup background colour
    glClearColor( 0, 0, 0, 1 );
    glEnable( GL_DEPTH_TEST );
    
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(keyboardSpecial);
    
    glutMainLoop();
    
    return 0;
}