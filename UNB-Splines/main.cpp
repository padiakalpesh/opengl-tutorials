//
//  main.cpp
//  UNB-Splines
//
//  Created by Kalpesh Padia on 10/25/15.
//  Copyright (c) 2015 Kalpesh Padia. All rights reserved.
//

#include <cstdio>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include <cmath>
#include <vector>

#define MAX_T 20

using namespace std;

int left = -20;
int right = 20;
int top = 20;
int bottom  = -20;

float edgeLength = 600;

vector < vector<float> > points;

int timeSteps = 0;
int jLimit = 0;

void display_obj();

void timer(int val)
{
    ++timeSteps;
    glutPostRedisplay();
}

void drawSegment(vector<float> p1, vector<float> r1, vector<float> r4, vector<float> p4, bool full)
{
    float x, y, z;
    float delta = 1.0/MAX_T;
    float t;
    int ts;
    
    glLineWidth(3.0);
    glBegin(GL_LINE_STRIP);
    
    if (full)
        ts = MAX_T;
    else
        ts = timeSteps % (MAX_T + 1);
    
    //using float causes error in precision
    for (int i=0; i<= ts; i++)
    {
        t = delta * i;
        
        glColor3f(0,0,0);
        x = (pow(1-t,3)/6.0 * p1[0]) + ((3*pow(t,3) - 6*pow(t,2) + 4)/6.0 * r1[0]) + ((-3*pow(t,3) + 3*pow(t,2) + 3*t + 1)/6.0 * r4[0]) + (pow(t,3)/6.0 * p4[0]);
        y = (pow(1-t,3)/6.0 * p1[1]) + ((3*pow(t,3) - 6*pow(t,2) + 4)/6.0 * r1[1]) + ((-3*pow(t,3) + 3*pow(t,2) + 3*t + 1)/6.0 * r4[1]) + (pow(t,3)/6.0 * p4[1]);
        z = 3.0;
        
        glVertex3f(x, y, z);
    }
    
    glEnd();
    
    
    //knot points
    glPointSize(6.0);
    glBegin(GL_POINTS);
    glColor3f(1,0.3,0.7);
    t = 0;
    x = (pow(1-t,3)/6.0 * p1[0]) + ((3*pow(t,3) - 6*pow(t,2) + 4)/6.0 * r1[0]) + ((-3*pow(t,3) + 3*pow(t,2) + 3*t + 1)/6.0 * r4[0]) + (pow(t,3)/6.0 * p4[0]);
    y = (pow(1-t,3)/6.0 * p1[1]) + ((3*pow(t,3) - 6*pow(t,2) + 4)/6.0 * r1[1]) + ((-3*pow(t,3) + 3*pow(t,2) + 3*t + 1)/6.0 * r4[1]) + (pow(t,3)/6.0 * p4[1]);
    z = 3.0;
    glVertex3f(x, y, z);
    
    t = 1;
    x = (pow(1-t,3)/6.0 * p1[0]) + ((3*pow(t,3) - 6*pow(t,2) + 4)/6.0 * r1[0]) + ((-3*pow(t,3) + 3*pow(t,2) + 3*t + 1)/6.0 * r4[0]) + (pow(t,3)/6.0 * p4[0]);
    y = (pow(1-t,3)/6.0 * p1[1]) + ((3*pow(t,3) - 6*pow(t,2) + 4)/6.0 * r1[1]) + ((-3*pow(t,3) + 3*pow(t,2) + 3*t + 1)/6.0 * r4[1]) + (pow(t,3)/6.0 * p4[1]);
    z = 3.0;
    glVertex3f(x, y, z);
    glEnd();
    
}

void drawGrid()
{
    glEnable(GL_LINE_SMOOTH);
    
    glColor4f(.9,.9,.9,1);
    glLineWidth(1.0);
    
    glBegin(GL_LINES);
    for (int i=left+1; i<right-1; i++)
    {
        if (i == left+1)
            continue;
        glVertex3f(i, bottom+1, 0);
        glVertex3f(i, top-1, 0);
    }
    
    for (int i=bottom+1; i<top-1; i++)
    {
        if (i == bottom+1)
            continue;
        glVertex3f(left+1, i, 0);
        glVertex3f(right-1, i, 0);
    }
    
    glColor4f(.8,.8,.8,1);
    glLineWidth(2);
    glVertex3f(left+1, 0, 0);
    glVertex3f(right-1, 0, 0);
    
    glVertex3f(0, bottom+1, 0);
    glVertex3f(0, top-1, 0);
    
    
    glLineWidth(5.0);
    
    glColor4f(0,1,0,1);
    glVertex3f(left+1, bottom+1, 0);
    glVertex3f(right-1, bottom+1, 0);
    
    glVertex3f(right-1, bottom+1, 0);
    glVertex3f(right-1, top-1, 0);
    
    glVertex3f(right-1, top-1, 0);
    glVertex3f(left+1, top-1, 0);
    
    glVertex3f(left+1, top-1, 0);
    glVertex3f(left+1, bottom+1, 0);
    
    glEnd();
}

void display()
{
    glClearColor( 0.95, 0.95, 0.95, 1 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    drawGrid();
    
    glPointSize(8.0);
    glBegin(GL_POINTS);
    glColor3f(0,0,0);
    for (int i=0; i<points.size(); i++)
    {
        glVertex3f(points[i][0], points[i][1], points[i][2]);
    }
    glEnd();
    
    glLineWidth(1.0);
    glBegin(GL_LINE_STRIP);
	glColor3f(.5,0,1);
    for (int i=0; i<points.size(); i++)
    {
        glVertex3f(points[i][0], points[i][1], points[i][2]);
    }
    glEnd();
    
    if (points.size() > 3)
    {
        for (int i = 3; i < points.size() - 1; i++)
            drawSegment(points[i-3], points[i-2], points[i-1], points[i], true);

        int i = points.size()-1;
        
        for (int j = 0; j <= timeSteps; j++)
        {
            drawSegment(points[i-3], points[i-2], points[i-1], points[i], false);
        }
        
        if (timeSteps <  MAX_T)
        {
            glutTimerFunc(20, timer, 0);
        }
    }
    
    glFlush();
    glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
    if (key == 27)
    {
        printf("Clear");
        points.clear();
        glutPostRedisplay();
    }
}

void mouse(int button, int state, int x, int y)
{
    int viewport[4] = {0, 0, edgeLength, edgeLength};
    double modelview[16];
    double projection[16];
    
    float winX, winY, winZ;
    double posX, posY, posZ;
    
    vector<float> pos;
    
    glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
    glGetDoublev( GL_PROJECTION_MATRIX, projection );
    //glGetIntegerv( GL_VIEWPORT, viewport );
    
    if (button == GLUT_LEFT_BUTTON && state == GLUT_UP)
    {
        
        gluUnProject( (float)(x), (float)(600-y), 0.5, modelview, projection, viewport, &posX, &posY, &posZ);
        printf("%d %d, %f %f %f \n", x, y, posX, posY, posZ);
        
        if (posX <= left+1 || posX >= right-1 || posY <= bottom+1 || posY >= top-1)
            return;
        
        pos.push_back(posX);
        pos.push_back(posY);
        pos.push_back(3.0);
        
        points.push_back(pos);
        timeSteps = 0;
        
        glutPostRedisplay();
    }
}

int main(int argc, char** argv)
{
    glutInit( &argc, argv );		// Initialize GLUT
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
    glutInitWindowSize( edgeLength, edgeLength );
    glutCreateWindow( "UNB-Splines Demo" );
    
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    glOrtho(left, right, bottom, top, 3, -3);
    
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();
    
    glutDisplayFunc( display );	// Setup GLUT callbacks
    glutKeyboardFunc(keyboard);
    glutMouseFunc(mouse);
    
    glutMainLoop();
    
    return 0;
}
