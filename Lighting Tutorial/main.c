//
//  main.c
//  OpenGL Lighting Tutorial
//
//  Created by Kalpesh Padia on 9/22/15.
//  Copyright (c) 2015 Kalpesh Padia. All rights reserved.
//

#include <stdio.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>

float eyeX = 5.0f;
float eyeY = 5.0f;
float eyeZ = 5.0f;

float upX = 0.0f;
float upY = 0.0f;
float upZ = 1.0f;

float lightPosX = 5.0f;
float lightPosY = 1.00f;
float lightPosZ = 3.0f;

int angle = 0;
int _refreshmilliseconds = (1 / 60.0) * 1000;

int model = 1;
int edgeLength = 1050;
int polyCount = 100;
int disp = 0;

int toggleRotate = 1;

float shininess = 8.0;

// Ambient light
//GLfloat ambientCol[] = { 21/255.0f, 84/255.0f, 237/255.0f, 1.0f };
GLfloat ambientCol[] = { 100/255.0f, 100/255.0f, 255/255.0f, 1.0f };

// Positioned light
GLfloat lightCol0[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightColBlack[] = {0.0f, 0.0f, 0.0f, 1.0f };

void timer(int value)
{
    glutPostRedisplay();
    
    if (toggleRotate) {
        angle += 2;
        angle %= 360;
    }
    
    glutTimerFunc(_refreshmilliseconds, timer, 0);
}

void draw_string(float x, float y, float z, char *txt)
{
    glColor3f(1,1,1);
    glRasterPos3f(x, y,z);
    while( *txt != '\0' ) {
        glutBitmapCharacter( GLUT_BITMAP_9_BY_15, *txt );
        txt++;
    }
}

void drawAxes()
{
    glBegin(GL_LINES);
    
    //X
    glColor4f(1,0,0,1);
    glVertex3f(0,0,0);
    glVertex3f(eyeX,0,0);
    
    //Y
    glColor4f(0,1,0,1);
    glVertex3f(0,0,0);
    glVertex3f(0,eyeY,0);
    
    //Z
    glColor4f(0,0,1,1);
    glVertex3f(0,0,0);
    glVertex3f(0,0,eyeZ);
    
    glEnd();
    
    draw_string(5.7, -0.1, 0, "+X");
    draw_string(0, 5.2, -0.2, "+Y");
    draw_string(0, -0.6, 5.2, "+Z");
}

void displayTitleString(float x, char *txt )
{
    glClearColor( 0, 0, 0, 1 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glMatrixMode( GL_MODELVIEW );		// Setup model transformations
    glLoadIdentity();
    gluLookAt(0, 0, 5, 0, 0, -5, 0, 1, 0);
    glPushMatrix();
    
    glColor3f(1,1,1);
    glRasterPos3f(x, 0, 0);
    while( *txt != '\0' ) {
        glutBitmapCharacter( GLUT_BITMAP_9_BY_15, *txt );
        txt++;
    }
    glPopMatrix();
    glFlush();
}

void display_obj(int lighting)
{
    glClearColor(0.33,0.33,0.33,1);
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glMatrixMode( GL_MODELVIEW );		// Setup model transformations
    glLoadIdentity();
    gluLookAt( eyeX, eyeY, eyeZ, -eyeX, -eyeY, -eyeZ, upX, upY, upZ);
    glPushMatrix();
    
    drawAxes();
    
    // Use lighting
    GLfloat lightPos0[] = { lightPosX, lightPosY, lightPosZ, 1.0f };
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    
    //add ambient light to scene
    //glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lightCol0);
    
    //shading
    glShadeModel(GL_SMOOTH);
    
    //reset all
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColBlack);
    glLightfv(GL_LIGHT0, GL_AMBIENT, lightColBlack);
    glLightfv(GL_LIGHT0, GL_SPECULAR, lightColBlack);
    
    switch (lighting) {
        case 2:
            glLightfv(GL_LIGHT0, GL_SPECULAR, lightCol0);
            break;
        case 1:
            glLightfv(GL_LIGHT0, GL_DIFFUSE, ambientCol);
            break;
        case 0:
            glLightfv(GL_LIGHT0, GL_AMBIENT, ambientCol);
            break;
        default:
            glLightfv(GL_LIGHT0, GL_SPECULAR, lightCol0);
            glLightfv(GL_LIGHT0, GL_DIFFUSE, ambientCol);
            glLightfv(GL_LIGHT0, GL_AMBIENT, ambientCol);
            break;
    }
    
    //light position
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos0);
    
    // add material properties
    //glMaterialfv(GL_FRONT, GL_AMBIENT, ambientCol);
    glMaterialfv(GL_FRONT, GL_SPECULAR, lightCol0);
    glMaterialf(GL_FRONT, GL_SHININESS, shininess);
    
    
    // Keyboard-controlled circle, player1
    glPushMatrix();
    glRotatef(angle, 0.0f, 0.0f, 1.0f);
    
    switch (model) {
        case 2: glutSolidCone(3.0f, 3.0f, polyCount, polyCount);
            break;
        case 3: glutSolidTorus(1.0f, 3.0f, polyCount, polyCount);
            break;
        case 4: glutSolidTeapot(3.0f);
            break;
        default:
            glutSolidSphere(4.0f, polyCount, polyCount);
            break;
    }
    
    glPopMatrix();
    glDisable(GL_LIGHTING);

    
    //show where light is
    glPushMatrix();
    glTranslatef(lightPosX, lightPosY, lightPosZ);
    glutSolidSphere(0.1,100, 100);
    glPopMatrix();
    
    glFlush();
}

void display()
{
    glClearColor( 0, 0, 0, 1 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glEnable(GL_SCISSOR_TEST);
    char str[30];
    switch (disp) {
        case 4:
            //bottom right
            //all
            glViewport(.533*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            glScissor(.533*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            displayTitleString(-5.7, "Ambient + Diffuse + Specular Lighting");
            
            glViewport(.533*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            glScissor(.533*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            display_obj(3);

            sprintf(str, "Shininess = %0.2f", shininess);
            draw_string(-3,0,7,str);
            
            sprintf(str, "%d Slices, %d Stacks", polyCount, polyCount);
            draw_string(18,-10,-20,str);
    	
        case 3:
            //bottom left
            //only specular
            glViewport(0.066*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            glScissor(0.066*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            displayTitleString(-3, "Specular Lighting");
            
            glViewport(0.066*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            glScissor(0.066*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            display_obj(2);
            
            sprintf(str, "%d Slices, %d Stacks", polyCount, polyCount);
            draw_string(18,-10,-20,str);
    
        case 2:
            //top right
            //only diffuse
            glViewport(.533*edgeLength, 0.934*edgeLength, .4*edgeLength, .066*edgeLength);
            glScissor(.533*edgeLength, 0.934*edgeLength, .4*edgeLength, .066*edgeLength);
            displayTitleString(-2.5, "Diffuse Lighting");
            
            glViewport(.533*edgeLength, .534*edgeLength, .4*edgeLength, .4*edgeLength);
            glScissor(.533*edgeLength, .534*edgeLength, .4*edgeLength, .4*edgeLength);
            display_obj(1);
            
            sprintf(str, "%d Slices, %d Stacks", polyCount, polyCount);
            draw_string(18,-10,-20,str);
    
        case 1:
            //top left
            //No shading (only ambient light)
            glViewport(0.066*edgeLength, 0.934*edgeLength, .4*edgeLength, .066*edgeLength);
            glScissor(0.066*edgeLength, 0.934*edgeLength, .4*edgeLength, .066*edgeLength);
            displayTitleString(-3, "Ambient Lighting");
            
            glViewport(0.066*edgeLength, .534*edgeLength, .4*edgeLength, .4*edgeLength);
            glScissor(0.066*edgeLength, .534*edgeLength, .4*edgeLength, .4*edgeLength);
            display_obj(0);
            
            sprintf(str, "Light at (%0.2f, %0.2f, %0.2f)", lightPosX, lightPosY, lightPosZ);
            draw_string(-3,-62,20,str);
            sprintf(str, "%d Slices, %d Stacks", polyCount, polyCount);
            draw_string(18,-10,-20,str);
            
        default:
            break;
    }
    
    glDisable(GL_SCISSOR_TEST);
    
	glutSwapBuffers();
}

void setupDisplay()
{
    glMatrixMode( GL_PROJECTION );	// Setup perspective projection
    glLoadIdentity();
    gluPerspective( 110, 1, 1, 40 );
    
    glMatrixMode( GL_MODELVIEW );		// Setup model transformations
    glLoadIdentity();
    gluLookAt( eyeX, eyeY, eyeZ, -eyeX, -eyeY, -eyeZ, upX, upY, upZ);
}

void keyboard(unsigned char c, int x, int y){
    
    switch (c) {
        case 'Z':
        case 'z':
            lightPosX += 0.05;
            break;
		case 'X':
        case 'x':
            lightPosX -= 0.05;
            break;

        case 'L':
        case 'l':
            lightPosX = 5.0f;
            lightPosY = 1.0f;
            lightPosZ = 3.0f;
            break;
        
        case '=':
            shininess += 0.1;
            if (shininess >=128)
                shininess = 128;
            break;
        case '-':
            shininess -= 0.1;
            if (shininess <=1)
                shininess = 1;
            break;
        case ' ':
            disp = (++disp) % 5;
            break;
        case ']':
            polyCount++;
            break;
        case '[':
            polyCount = (polyCount == 2)? polyCount : polyCount - 1;
            break;
        
        case 'R':
        case 'r':
            toggleRotate = (++toggleRotate)%2;
            break;
        default:
            break;
    }
    
    printf("%0.2f, %0.2f, %0.2f\n", lightPosX, lightPosY, lightPosZ);
    
    glutPostRedisplay();
}

void keyboardSpecial(int key, int x, int y) {
    
    switch (key) {
        case GLUT_KEY_RIGHT:
            lightPosY += 0.05;
            break;
        case GLUT_KEY_LEFT:
            lightPosY -= 0.05;
            break;
        case GLUT_KEY_UP:
            lightPosZ += 0.05;
            break;
        case GLUT_KEY_DOWN:
            lightPosZ -= 0.05;
            break;
        default:
            break;
    }
    printf("%0.2f, %0.2f, %0.2f\n", lightPosX, lightPosY, lightPosZ);
    
    glutPostRedisplay();
}

void menu(int value)
{
    model = value;
    printf("model = %d", model);
    // you would want to redraw now
    glutPostRedisplay();
}

void createMenu()
{
    int contextMenu = glutCreateMenu(menu);
    
    glutAddMenuEntry("Sphere",1);
    glutAddMenuEntry("Cone",2);
    glutAddMenuEntry("Torus",3);
    glutAddMenuEntry("Teapot",4);
    glutAttachMenu(GLUT_RIGHT_BUTTON);
}

int main(int argc, char** argv)
{
    glutInit( &argc, argv );		// Initialize GLUT
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
    glutInitWindowSize( edgeLength, edgeLength );
    glutCreateWindow( "Lighting Demo" );
    
    setupDisplay();
    createMenu();
    
    glClearDepth( 1.0 );			// Setup background colour
    glClearColor( 0.33, 0.33, 0.33, 1 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glEnable( GL_DEPTH_TEST );
    
    glutDisplayFunc(display);	// Setup GLUT callbacks
    glutTimerFunc(0, timer, 0);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(keyboardSpecial);
    
    glutMainLoop();
    
    return 0;
}