//
//  main.c
//  OpenGL Line Drawing Tutorial
//
//  Created by Kalpesh Padia on 9/30/15.
//  Copyright (c) 2015 Kalpesh Padia. All rights reserved.
//

#include <cstdio>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include <cstdlib>

#define slope 0.65

int draw = 0;

int eyeX = 0;
int eyeY = 0;
int eyeZ = 5;

int iNE, iE, d, y;

float x0 = 1;
float x2 = 29;
float x1 = x0+1;

float y0 = 1;
float y2 = ((slope * x2) - ((int)(slope * x2)) < 0.5)?((int)(slope * x2)):((int)(slope * x2)+1);
float y1 = y2/x2 * x1;

float x = x0;

int prevState = GLUT_UP;
int prevPosX;
int prevPosY;
float translateX = 0;
float translateY = 0;

int togglePoints = 1;

float scale = 0.55;

void timer(int value)
{
    glutPostRedisplay();
    
    glutTimerFunc(50, timer, 0);
}


void drawGrid()
{
    glEnable(GL_LINE_SMOOTH);
    
    glColor4f(.33,.33,.33,1);
    glLineWidth(1);
    
    glBegin(GL_LINES);
    for (int i=0; i<=x2+1; i++)
    {
        if (i == 0)
            continue;
        glVertex3f(i, 0, 0);
        glVertex3f(i, x2+1, 0);
        
        glVertex3f(0, i, 0);
        glVertex3f(x2+1, i, 0);
        
    }
    
    glLineWidth(5);
    
    glColor4f(0,1,0,1);
    glVertex3f(0, 0, 0);
    glVertex3f(x2+1, 0, 0);
    
    glVertex3f(0, x2+1, 0);
    glVertex3f(x2+1, x2+1, 0);
    
    glVertex3f(0, 0, 0);
    glVertex3f(0, x2+1, 0);
    
    glVertex3f(x2+1, 0, 0);
    glVertex3f(x2+1, x2+1, 0);
    
    glEnd();
}

void display_obj()
{
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glMatrixMode( GL_MODELVIEW );		// Setup model transformations
    glPushMatrix();			// Save glLookAt transformation
    
    glScalef(scale, scale, 0);
	glTranslatef(-x2/2 - 0.5, -x2/2 - 0.5, 0);
    gluLookAt( eyeX, eyeY, eyeZ, eyeX, eyeY, -1, 0, 1, 0 );
    
    drawGrid();
    
    //draw a line
    
    glLineWidth(2);
    
    glBegin(GL_LINES);
    	glColor4f(1,1,1,1);
    	glVertex3f(x0, y0, 0);
    	glVertex3f(x2, y2, 0);
    glEnd();
    
    
    //draw line using points
    glColor4f(1,0,0,1);
    
    //d = 2dy - dx
    //iNE = 2dy-2dx
    //iE = 2dy;
    d = 2*(y2-y0) - (x2-x0);
    iNE = 2*(y2-y0) - 2*(x2-x0);
    iE = 2*(y2-y0);
    y = y0;
    
    glBegin(GL_LINE_STRIP);
    
    for (int i=x0+1; i<=x1; i++)
    {
        //previous point
        glVertex3f(i-1, y, 0);
        
        //decision for this point (drawn in next iteration)
        if (d > 0)
        {
            y++;
            d += iNE;
        }
        else
            d += iE;
    }
    //draw the final point
    if (x1 == x2)
        glVertex3f(x1, y, 0);
    
    glEnd();
    if (togglePoints)
    {
        //mark points over line
        glPointSize(7.5);
        glBegin(GL_POINTS);
        
        d = 2*(y2-y0) - (x2-x0);
        iNE = 2*(y2-y0) - 2*(x2-x0);
        iE = 2*(y2-y0);
        y = y0;
        
        //next mid-point
        glColor4f(0x33/255.,0xff/255.,0,1);
        glVertex3f(x0+1, y+0.5, 0);
        
        for (int i=x0+1; i<=x1; i++)
        {
            //mark the mid-point for this iteration (current mid point) as having evaluated
            //do not mark anything for the very first point
            if (i < x1)
            {
                glColor4f(0xff/255.,0x00/255.,0xb2/255.,1);
                glVertex3f(i, y+0.5, 0);
                
            }
            
            //commit previous decision; for i = x0+1, this is the first point.
            glColor4f(0,1,1,1);
            glVertex3f(i-1, y, 0);
            
            //new decision based on current mid point
            if (d > 0)
            {
                y++;
                d += iNE;
            }
            else
            {
                d += iE;
            }
            
            // mark the location of the next mid-point
            if (i < x1 && x1 != x2)
            {
                glColor4f(0x33/255.,0xff/255.,0,1);
                glVertex3f(i+1, y+0.5, 0);
                
            }
        }
        
        //draw the final point
        if (x1 == x2)
            glVertex3f(x1, y, 0);
        
        glEnd();
    }
    
    glDisable(GL_LINE_SMOOTH);
    
    glPopMatrix();
    glFlush();
    glutSwapBuffers();
}

void keyboard(unsigned char c, int x, int y)
{
    if (c == 32)
    {
        x1++;
        if (x1 > x2)
            x1 = x0+1;
        
        y1 = y2/x2 * x1;
    }
    
    if (c == 'Z' || c == 'z')
    {
        //eyeZ--;
        scale += 0.02;
    }
    
    if (c == 'x' || c == 'X')
    {
        //eyeZ++;
        if (scale - 0.02 < 0.02)
            scale = 0.02;
        else
            scale -= 0.02;
    }
    
    if (c == 't' || c == 'T')
    {
        togglePoints = (++togglePoints) % 2;
    }
    glutPostRedisplay();
}

void keyboardSpecial(int key, int x, int y) {
    
    switch (key) {
        case GLUT_KEY_RIGHT:
            eyeX++;
            break;
        case GLUT_KEY_LEFT:
            eyeX--;
            break;
        case GLUT_KEY_UP:
            eyeY++;
            break;
        case GLUT_KEY_DOWN:
            eyeY--;
            break;
            
        default:
            break;
    }
    
    glutPostRedisplay();
}

int main(int argc, char** argv)
{
    glutInit( &argc, argv );		// Initialize GLUT
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
    glutInitWindowSize( 600, 600 );
    glutCreateWindow( "Line Drawing Demo" );
    
    glutDisplayFunc( display_obj );	// Setup GLUT callbacks
    
    glMatrixMode( GL_PROJECTION );	// Setup perspective projection
    glLoadIdentity();
    gluPerspective( 120, 1, 1, 40 );
    
    glMatrixMode( GL_MODELVIEW );		// Setup model transformations
    glLoadIdentity();
    
    gluLookAt( eyeX, eyeY, eyeZ, eyeX, eyeY, -eyeZ, 0, 1, 0 );
    
    glShadeModel( GL_SMOOTH );
    
    glClearDepth( 1.0 );			// Setup background colour
    glClearColor( 0, 0, 0, 1 );
    //glEnable( GL_DEPTH_TEST );
    
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(keyboardSpecial);
    
    //glutTimerFunc(50, timer, 0);
    
    glutMainLoop();
    
    return 0;
}