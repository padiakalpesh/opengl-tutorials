//
//  main.c
//  OpenGL Color Model Tutorial
//
//  Created by Kalpesh Padia on 10/06/15.
//  Copyright (c) 2015 Kalpesh Padia. All rights reserved.
//

#include <stdio.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include <math.h>

int disp = 0;

int eyeX = 0;
int eyeY = 0;
int eyeZ = 5;

float delta = 0.01;
int edgeLength = 800;
int stacks = 4000;

//draws a circle of radius r, centered at origin using the midpoint circle algorithm
void Circle(float r)
{
    float x, y;
    float i = 0;
    
    for (i = 0; i < 360 ; i+=delta)
    {
        x = r * cos(M_PI * i /90.0);
        y = r * sin(M_PI * i /90.0);
        glVertex2f(x, y);
    }
    
}

void RGB()
{
    float radius = 3.0;
    //red
    glColor4f( 1, 0, 0, 1);
    glPushMatrix();
    glTranslatef(0,0.6*radius,0);
    
    glBegin(GL_POLYGON);
    Circle(radius);
    //glutSolidSphere(radius, stacks, stacks);
    glEnd();
    
    glPopMatrix();
    
    //green
    glColor4f( 0, 1, 0, 1);
    glPushMatrix();
    glTranslatef(0.6*radius,-0.3*radius,0);
    
    glBegin(GL_POLYGON);
    Circle(radius);
    //glutSolidSphere(radius, stacks, stacks);
    glEnd();
    
    glPopMatrix();
    
    //blue
    glColor4f( 0, 0, 1, 1);
    glPushMatrix();
    glTranslatef(-0.6*radius,-0.3*radius,0);
    
    glBegin(GL_POLYGON);
    Circle(radius);
    //glutSolidSphere(radius, stacks, stacks);
    glEnd();
    
    glPopMatrix();
}

void CMY()
{
    float radius = 3.0;
    //magenta
    glColor4f( 1, 0, 1, 1);
    glPushMatrix();
    glTranslatef(0,0.6*radius,0);
    
    glBegin(GL_POLYGON);
    Circle(radius);
    //glutSolidSphere(radius, stacks, stacks);
    glEnd();
    
    glPopMatrix();
    
    //yellow
    glColor4f( 1, 1, 0, 1);
    glPushMatrix();
    glTranslatef(0.6*radius,-0.3*radius,0);
    
    glBegin(GL_POLYGON);
    Circle(radius);
    //glutSolidSphere(radius, stacks, stacks);
    glEnd();
    
    glPopMatrix();
    
    //cyan
    glColor4f( 0, 1, 1, 1);
    glPushMatrix();
    glTranslatef(-0.6*radius,-0.3*radius,0);
    
    glBegin(GL_POLYGON);
    Circle(radius);
    //glutSolidSphere(radius, stacks, stacks);
    glEnd();
    
    glPopMatrix();

}

void displayTitleString(float x, char *txt )
{
    glMatrixMode( GL_MODELVIEW );		// Setup model transformations
    glLoadIdentity();
    gluLookAt(0, 0, 5, 0, 0, -5, 0, 1, 0);
    glPushMatrix();
    
    glColor3f(1,1,1);
    glRasterPos3f(x, 0, 0);
    while( *txt != '\0' ) {
        glutBitmapCharacter( GLUT_BITMAP_9_BY_15, *txt );
        txt++;
    }
    glPopMatrix();
    glFlush();
}


void display(colorModel)
{
    glMatrixMode( GL_MODELVIEW );		// Setup model transformations
    glLoadIdentity();
    gluLookAt( eyeX, eyeY, eyeZ, -eyeX, -eyeY, -eyeZ, 0, 1, 0 );
    glPushMatrix();			// Save glLookAt transformation
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE);
    
    switch (colorModel)
    {
        case 1:
            glClearColor( 0, 0, 0, 1 );
            glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

            glBlendEquation(GL_FUNC_ADD);
            RGB();
        break;
            
        case 2:
            glClearColor( 1, 1, 1, 1 );
            glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
            
            //Subtract the background color from this color. == Subtractive blending
            glBlendEquation(GL_FUNC_SUBTRACT);
            CMY();
            
            //Subtract this color from the background color. == negative additive blending
            //glBlendEquation(GL_FUNC_REVERSE_SUBTRACT);
            //RGB(); //since CMY = 1 - RGB and bkg color = 1
            break;
    }
    
    glDisable(GL_BLEND);

    glFlush();
}

void display_obj()
{
    glClearColor( 0.3, 0.3, 0.3, 1 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glEnable(GL_SCISSOR_TEST);
    switch (disp) {
        case 2:
            //bottom right
            //phong shading - low poly count
            glViewport(.533*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            glScissor(.533*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            displayTitleString(-0.8, "CMYK");
            
            glViewport(.533*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            glScissor(.533*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            display(2);
            
        case 1:
            //bottom left
            //gouraud shading - high poly count
            glViewport(0.066*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            glScissor(0.066*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            displayTitleString(-0.75, "RGB");
            
            glViewport(0.066*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            glScissor(0.066*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            display(1);

            
    	default:
            break;
    }
    
    glDisable(GL_SCISSOR_TEST);
    
    glutSwapBuffers();
}



void keyboard(unsigned char c, int x, int y){
    if (c == ' ')
    {
        disp = ++disp % 3;
    }
    
    if (c == 27)
    {
        disp = --disp % 3;
        
        if (disp < 0)
        {
            disp = 0;
        }
    }
    
    if (c == 'Z' || c == 'z')
    {
        eyeZ++;
    }
    
    if (c == 'x' || c == 'X')
    {
        eyeZ--;
    }
    
    display_obj();
}

void keyboardSpecial(int key, int x, int y) {
    
    switch (key) {
        case GLUT_KEY_RIGHT:
            eyeX++;
            break;
        case GLUT_KEY_LEFT:
            eyeX--;
            break;
        case GLUT_KEY_UP:
            eyeY++;
            break;
        case GLUT_KEY_DOWN:
            eyeY--;
            break;
            
        default:
            break;
    }
    
    display_obj();
}

int main(int argc, char** argv)
{
    glutInit( &argc, argv );		// Initialize GLUT
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
    glutInitWindowSize( edgeLength, edgeLength/2 );
    glutCreateWindow( "Color Demo" );
    
    glutDisplayFunc( display_obj );	// Setup GLUT callbacks
    
    glMatrixMode( GL_PROJECTION );	// Setup perspective projection
    glLoadIdentity();
    //gluPerspective( 110, 1, 1, 40 );
    glOrtho(-5,5,-5,5,5,-5);
    
    glMatrixMode( GL_MODELVIEW );		// Setup model transformations
    glLoadIdentity();
    //gluLookAt( 5, 5, 5, -5, -5, -5, 0, 1, 0 );
    gluLookAt( eyeX, eyeY, eyeZ, -eyeX, -eyeY, -eyeZ, 0, 1, 0 );
    
    glShadeModel( GL_SMOOTH );
    
    glClearDepth( 1.0 );			// Setup background colour
    glClearColor( 0, 0, 0, 1 );
    
    //glDepthFunc(GL_LEQUAL);
    //glEnable( GL_DEPTH_TEST );
    
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(keyboardSpecial);
    
    glutMainLoop();
    
    return 0;
}