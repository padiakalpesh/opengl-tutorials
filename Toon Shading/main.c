//
//  main.c
//  Toon Shading
//
//  Created by Kalpesh Padia on 11/08/15.
//  Copyright (c) 2015 Kalpesh Padia. All rights reserved.
//

#include <stdio.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>

float eyeX = 0.0f;
float eyeY = 3.0f;
float eyeZ = 8.0f;

float upX = 0.0f;
float upY = 1.0f;
float upZ = 0.0f;

float lightPosX = 3.30f;
float lightPosY = 2.90f;
float lightPosZ = 3.00f;

int angle = 0;
int _refreshmilliseconds = (1 / 60.0) * 1000;

int toggleRotate = 0;

int model = 1;
int edgeLength = 1600;
int disp = 0;

float shininess = 8.0;

int vertexshader,fragmentshader;
int shaderProgram;

// Ambient light
GLfloat ambientCol[] = { 231/255.0f, 20/255.0f, 30/255.0f, 1.0f };

// Positioned light
GLfloat lightCol0[] = { 1.0f, 1.0f, 1.0f, 1.0f };

void timer(int value)
{
    glutPostRedisplay();
    
    if (toggleRotate) {
        angle += 2;
        angle %= 360;
    }
    
    glutTimerFunc(_refreshmilliseconds, timer, 0);
}

void draw_string(float x, float y, float z, char *txt)
{
    glColor3f(1,1,1);
    glRasterPos3f(x, y,z);
    while( *txt != '\0' ) {
        glutBitmapCharacter( GLUT_BITMAP_9_BY_15, *txt );
        txt++;
    }
}

void displayTitleString(float x, char *txt )
{
    glClearColor( 0, 0, 0, 1 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glMatrixMode( GL_MODELVIEW );		// Setup model transformations
    glLoadIdentity();
    gluLookAt(0, 0, 5, 0, 0, -5, 0, 1, 0);
    glPushMatrix();
    
    glColor3f(1,1,1);
    glRasterPos3f(x, 0, 0);
    while( *txt != '\0' ) {
        glutBitmapCharacter( GLUT_BITMAP_9_BY_15, *txt );
        txt++;
    }
    glPopMatrix();
    glFlush();
}

void display_obj(int angle, int useShader)
{
    glClearColor(0.33,0.33,0.33,1);
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glMatrixMode( GL_MODELVIEW );		// Setup model transformations
    glLoadIdentity();
    gluLookAt( eyeX, eyeY, eyeZ, -eyeX, -eyeY, -eyeZ, upX, upY, upZ);
    glPushMatrix();
    
    // add material properties
    //glMaterialfv(GL_FRONT, GL_AMBIENT, ambientCol);
    glMaterialfv(GL_FRONT, GL_SPECULAR, lightCol0);
    glMaterialf(GL_FRONT, GL_SHININESS, shininess);
    
    // Use lighting
    GLfloat lightPos0[] = { lightPosX, lightPosY, lightPosZ, 1.0f };
    
    //ambient, diffuse and specular components of light
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambientCol);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, ambientCol);
    glLightfv(GL_LIGHT0, GL_SPECULAR, lightCol0);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos0);
    
    
    if (useShader)
    {
        glUseProgram(shaderProgram);
        glPushMatrix();
        glRotatef(angle, 0.0f, 1.0f, 0.0f);
        glutSolidTeapot(1.8f);
        glPopMatrix();
        glUseProgram(0);
        
        //for silhouettes
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);	//really the teapot has a clockwise winding. Hence the front is back and vice-versa
        
        glLineWidth(5.0);
        glColor3f(0,0,0);
        //glEnable(GL_LINE_SMOOTH);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        
        glPushMatrix();
        glRotatef(angle, 0.0f, 1.0f, 0.0f);
        glutSolidTeapot(1.8f);
        glPopMatrix();
        
        glDisable(GL_CULL_FACE);
        //glDisable(GL_LINE_SMOOTH);
        glLineWidth(1.0);

    }
    else
    {
        glEnable(GL_LIGHT0);
        glEnable(GL_LIGHTING);
        //glEnable(GL_NORMALIZE);
        glShadeModel(GL_SMOOTH);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        
        glPushMatrix();
        glRotatef(angle, 0.0f, 1.0f, 0.0f);
        glutSolidTeapot(1.8f);
        glPopMatrix();
        
        glDisable(GL_LIGHTING);
    }
    
    glUseProgram(0);
    
    glFlush();
    
}

void display()
{
    glClearColor( 0, 0, 0, 1 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glEnable(GL_SCISSOR_TEST);
    char str[30];
    switch (disp) {
        case 2:
            //bottom right
            //phong shading - low poly count
            glViewport(.533*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            glScissor(.533*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            displayTitleString(-2.5, "Toon/Cel Shading");
            
            glViewport(.533*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            glScissor(.533*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            display_obj(angle, 1);
            
        case 1:
            //bottom left
            //gouraud shading - high poly count
            glViewport(0.066*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            glScissor(0.066*edgeLength, 0*edgeLength, .4*edgeLength, .066*edgeLength);
            displayTitleString(-2.5, "OpenGL Shading");
            
            glViewport(0.066*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            glScissor(0.066*edgeLength, 0.066*edgeLength, .4*edgeLength, .4*edgeLength);
            display_obj(angle, 0);
            
        	break;
        default:
            break;
    }
    
    glDisable(GL_SCISSOR_TEST);
    glUseProgram(0);
    
    glutSwapBuffers();
}

void setupDisplay()
{
    glMatrixMode( GL_PROJECTION );	// Setup perspective projection
    glLoadIdentity();
    gluPerspective( 45, 1, 1, 40 );
    //glOrtho(-5,5, -5, 5, 5, -5);
    
    glMatrixMode( GL_MODELVIEW );		// Setup model transformations
    glLoadIdentity();
    gluLookAt( eyeX, eyeY, eyeZ, -eyeX, -eyeY, -eyeZ, upX, upY, upZ);
}

void keyboard(unsigned char c, int x, int y){
    
    switch (c) {
        case 'Z':
        case 'z':
            lightPosZ += 0.05;
            break;
        case 'X':
        case 'x':
            lightPosZ -= 0.05;
            break;
            
        case 'L':
        case 'l':
            lightPosX = 5.0f;
            lightPosY = 1.0f;
            lightPosZ = 3.0f;
            break;
        case '=':
            shininess += 0.1;
            if (shininess >=128)
                shininess = 128;
            break;
        case '-':
            shininess -= 0.1;
            if (shininess <=1.0)
                shininess = 1.0;
            break;
        case ' ':
            disp =  (++disp) % 3;
            break;
        case 'R':
        case 'r':
            toggleRotate = (++toggleRotate)%2;
            break;
        default:
            break;
    }
    
    printf("%0.2f, %0.2f, %0.2f\n", lightPosX, lightPosY, lightPosZ);
}

void keyboardSpecial(int key, int x, int y) {
    
    switch (key) {
        case GLUT_KEY_RIGHT:
            lightPosX += 0.05;
            break;
        case GLUT_KEY_LEFT:
            lightPosX -= 0.05;
            break;
        case GLUT_KEY_UP:
            lightPosY += 0.05;
            break;
        case GLUT_KEY_DOWN:
            lightPosY -= 0.05;
            break;
        default:
            break;
    }
    printf("%0.2f, %0.2f, %0.2f\n", lightPosX, lightPosY, lightPosZ);
}

char *textFileRead(char *fn)
{
    FILE *fp;
    char *content = NULL;
    
    int count=0;
    
    if (fn != NULL) {
        fp = fopen(fn,"rt");
        
        if (fp != NULL) {
            
            fseek(fp, 0, SEEK_END);
            count = ftell(fp);
            rewind(fp);
            
            if (count > 0) {
                content = (char *)malloc(sizeof(char) * (count+1));
                count = fread(content,sizeof(char),count,fp);
                content[count] = '\0';
            }
            fclose(fp);
        }
    }
    return content;
}

void setupShaders()
{
    char *vs, *fs;
    
    // get a shader handler
    vertexshader = glCreateShader(GL_VERTEX_SHADER);
    // read the shader source from a file
    vs = textFileRead("vertexshader.txt");
    // conversions to fit the next function
    const char *vv = vs;
    // pass the source text to GL
    glShaderSource(vertexshader, 1, &vv,NULL);
    // free the memory from the source text
    free(vs);
    // finally compile the shader
    glCompileShader(vertexshader);
    
    // get a shader handler
    fragmentshader = glCreateShader(GL_FRAGMENT_SHADER);
    // read the shader source from a file
    fs = textFileRead("fragmentshader.txt");
    // conversions to fit the next function
    const char *fv = fs;
    // pass the source text to GL
    glShaderSource(fragmentshader, 1, &fv,NULL);
    // free the memory from the source text
    free(fs);
    // finally compile the shader
    glCompileShader(fragmentshader);
    
    shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram,vertexshader);
    glAttachShader(shaderProgram,fragmentshader);
    glLinkProgram(shaderProgram);
}

int main(int argc, char** argv)
{
    glutInit( &argc, argv );		// Initialize GLUT
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
    glutInitWindowSize( edgeLength, edgeLength/2 );
    glutCreateWindow( "Shading Demo" );
    
    setupDisplay();
    setupShaders();
    
    glClearDepth( 1.0 );			// Setup background colour
    glClearColor( 0.0, 0.0, 0.0, 1 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glEnable( GL_DEPTH_TEST );
    
    glutDisplayFunc(display);	// Setup GLUT callbacks
    glutTimerFunc(0, timer, 0);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(keyboardSpecial);
    
    glutMainLoop();
    
    return 0;
}