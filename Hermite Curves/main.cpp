//
//  main.cpp
//  Hermite Curves Tutorial
//
//  Created by Kalpesh Padia on 10/20/15.
//  Copyright (c) 2015 Kalpesh Padia. All rights reserved.
//

#include <cstdio>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include <cmath>

#define MAX_T 20

int left = -20;
int right = 20;
int top = 20;
int bottom  = -20;

float edgeLength = 600;

int pointCount = 0;

float P1[3];
float P4[3];
float R1[3];
float R4[3];

int steps = 0;
int timeSteps = 1;

void display_obj();

void timer(int val)
{
    timeSteps++;
    if (timeSteps <=MAX_T)
    {
        glutTimerFunc(50, timer, 0);
        //display_obj();
        //glutSwapBuffers();
        glutPostRedisplay();
    }
}

void drawSegments(float *p1, float *p4, float *r1, float *r4, int func)
{
    float x, y, z;
    float delta = 1.0/MAX_T;
    
    glBegin(GL_LINE_STRIP);
    //using float causes error in precision
    for (int i=0; i<=timeSteps; i++)
    {
        float t = delta * i;
        
        switch (func) {
            case 5:
                glColor3f(0,0,1);
                x  = (-2*pow(t,3) + 3*pow(t,2)) * p4[0] + (pow(t,3) - pow(t,2)) * r4[0];
                y  = (-2*pow(t,3) + 3*pow(t,2)) * p4[1] + (pow(t,3) - pow(t,2)) * r4[1];
                z = 0;
                break ;
            case 4:
                glColor3f(0,1,0);
                x  = (2*pow(t,3) - 3*pow(t,2) + 1) * p1[0]  + (pow(t,3) - 2*pow(t,2) + t) * r1[0];
                y  = (2*pow(t,3) - 3*pow(t,2) + 1) * p1[1]  + (pow(t,3) - 2*pow(t,2) + t) * r1[1];
                z = 0;
                break;
            case 3:
                glColor3f(1,0,0);
                x  = (-2*pow(t,3) + 3*pow(t,2)) * p4[0] + (2*pow(t,3) - 3*pow(t,2) + 1) * p1[0];
                y  = (-2*pow(t,3) + 3*pow(t,2)) * p4[1] + (2*pow(t,3) - 3*pow(t,2) + 1) * p1[1];
                z = 0;
                break;
            case 2:
                glColor3f(1,0,0);
                x  = (-2*pow(t,3) + 3*pow(t,2)) * p4[0];
                y  = (-2*pow(t,3) + 3*pow(t,2)) * p4[1];
                z = 0;
                break;
            case 1:
                glColor3f(1,0,0);
                x  = (2*pow(t,3) - 3*pow(t,2) + 1) * p1[0];
                y  = (2*pow(t,3) - 3*pow(t,2) + 1) * p1[1];
                z = 0;
                break;
            case 0:
                break;
            default:
                glColor3f(0,0,0);
                x = (2*pow(t,3) - 3*pow(t,2) + 1) * p1[0] + (-2*pow(t,3) + 3*pow(t,2)) * p4[0] + (pow(t,3) - 2*pow(t,2) + t) * r1[0] + (pow(t,3) - pow(t,2)) * r4[0];
                y = (2*pow(t,3) - 3*pow(t,2) + 1) * p1[1] + (-2*pow(t,3) + 3*pow(t,2)) * p4[1] + (pow(t,3) - 2*pow(t,2) + t) * r1[1] + (pow(t,3) - pow(t,2)) * r4[1];
                z = 3;
                break;
        }
        
        glVertex3f(x, y, z);
    }
    
    glEnd();
}

void drawHermite(float *p1, float *p4, float *r1, float *r4)
{
    glLineWidth(1.0);
    switch (steps) {
        case 6:
            drawSegments(p1, p4, r1, r4, 5);
            drawSegments(p1, p4, r1, r4, 4);
            drawSegments(p1, p4, r1, r4, 3);
            glLineWidth(3.0);
            drawSegments(p1, p4, r1, r4, steps);
            break;
        case 5:
            drawSegments(p1, p4, r1, r4, 5);
            break;
        case 4:
            drawSegments(p1, p4, r1, r4, 4);
            break;
        case 3:
            drawSegments(p1, p4, r1, r4, 3);
            break;
        case 2:
            drawSegments(p1, p4, r1, r4, 2);
            break;
        case 1:
            drawSegments(p1, p4, r1, r4, 1);
            break;
        case 0:
            break;

        default:
            glLineWidth(3.0);
            drawSegments(p1, p4, r1, r4, steps);
            break;
    }
}

void drawGrid()
{
    glEnable(GL_LINE_SMOOTH);
    
    glColor4f(.9,.9,.9,1);
    glLineWidth(1.0);
    
    glBegin(GL_LINES);
    for (int i=left+1; i<right-1; i++)
    {
        if (i == left+1)
            continue;
        glVertex3f(i, bottom+1, 0);
        glVertex3f(i, top-1, 0);
    }
    
    for (int i=bottom+1; i<top-1; i++)
    {
        if (i == bottom+1)
            continue;
        glVertex3f(left+1, i, 0);
        glVertex3f(right-1, i, 0);
    }
    
    glColor4f(.8,.8,.8,1);
    glLineWidth(2);
    glVertex3f(left+1, 0, 0);
    glVertex3f(right-1, 0, 0);
    
    glVertex3f(0, bottom+1, 0);
    glVertex3f(0, top-1, 0);

    
    glLineWidth(5.0);
    
    glColor4f(0,1,0,1);
    glVertex3f(left+1, bottom+1, 0);
    glVertex3f(right-1, bottom+1, 0);
    
    glVertex3f(right-1, bottom+1, 0);
    glVertex3f(right-1, top-1, 0);
    
    glVertex3f(right-1, top-1, 0);
    glVertex3f(left+1, top-1, 0);
    
    glVertex3f(left+1, top-1, 0);
    glVertex3f(left+1, bottom+1, 0);
    
    glEnd();
}

void display_obj()
{
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    glOrtho(left, right, bottom, top, 3, -3);
    
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();
    
    drawGrid();
    
    
    switch (pointCount) {
        case 4:
            glLineWidth(1.0);
            glBegin(GL_LINES);
            glColor3f(.5,0,1);
            glVertex3fv(P4);
            glVertex3fv(R4);
            glEnd();
            
            glPointSize(5.0);
            glBegin(GL_POINTS);
            glColor3f(1,.3,.5);
            glVertex3fv(R4);
            glEnd();
            
            drawHermite(P1, P4, R1, R4);
            
        case 3:
            glPointSize(8.0);
            glBegin(GL_POINTS);
            glColor3f(0,0,0);
            glVertex3fv(P4);
            glEnd();
        case 2:
            glLineWidth(1.0);
            glBegin(GL_LINES);
            glColor3f(.5,0,1);
            glVertex3fv(P1);
            glVertex3fv(R1);
            glEnd();
            
            glPointSize(5.0);
            glBegin(GL_POINTS);
            glColor3f(1,.3,.5);
            glVertex3fv(R1);
            glEnd();
        case 1:
            glPointSize(8.0);
            glBegin(GL_POINTS);
            glColor3f(0,0,0);
            glVertex3fv(P1);
            glEnd();
            break;
        default:
            break;
    }
    
    glFlush();
}

void display_xt()
{
    float x, y, z;
    float delta = 1.0/MAX_T;
    
    glClearColor( 0.33, 0.33, 0.33, 1 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    if (pointCount != 4)
        return;
    
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    //glOrtho(-0.2, 1.2, -0.8, 1.8, 3, -3);
    glOrtho(left, right, -0.2, 1.2, 3, -3);
    
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();
    
    glColor4f(.55,.55,.55,1);
    glLineWidth(2);
    
    glBegin(GL_LINES);
    glVertex3f(left+1, 0, 0);
    glVertex3f(right-1, 0, 0);
    
    glVertex3f(0, 0, 0);
    glVertex3f(0, 1, 0);
    
    //glVertex3f(0, 0, 0);
    //glVertex3f(0, 1.6, 0);
    
    glEnd();
    
    
    
    glColor3f(1,1,1);
    glBegin(GL_LINE_STRIP);
    //using float causes error in precision
    for (int i=0; i<=MAX_T; i++)
    {
        float t = delta * i;
        y = t;
        x = (2*pow(t,3) - 3*pow(t,2) + 1) * P1[0] + (-2*pow(t,3) + 3*pow(t,2)) * P4[0] + (pow(t,3) - 2*pow(t,2) + t) * R1[0] + (pow(t,3) - pow(t,2)) * R4[0];
        z = 3;
        printf("\n%f %f\n", x, y);
        glVertex3f(x, y, z);
    }
    
    glEnd();

}

void display_yt()
{
    float x, y, z;
    
    float delta = 1.0/MAX_T;
    
    glClearColor( 0.33, 0.33, 0.33, 1 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    if (pointCount != 4)
        return;
    
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    //glOrtho(-0.2, 1.2, -0.8, 1.8, 3, -3);
    glOrtho(-0.2, 1.2, bottom, top, 3, -3);
    
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();
    
    glColor4f(.55,.55,.55,1);
    glLineWidth(2);
    
    glBegin(GL_LINES);
    glVertex3f(0, 0, 0);
    glVertex3f(1, 0, 0);
    
    glVertex3f(0, bottom+1, 0);
    glVertex3f(0, top-1, 0);
    
    //glVertex3f(0, 0, 0);
    //glVertex3f(0, 1.6, 0);
    
    glEnd();
    
    
    
    glColor3f(1,1,1);
    glBegin(GL_LINE_STRIP);
    //using float causes error in precision
    for (int i=0; i<=MAX_T; i++)
    {
        float t = delta * i;
        x = t;
        y = (2*pow(t,3) - 3*pow(t,2) + 1) * P1[1] + (-2*pow(t,3) + 3*pow(t,2)) * P4[1] + (pow(t,3) - 2*pow(t,2) + t) * R1[1] + (pow(t,3) - pow(t,2)) * R4[1];
        z = 3;
        printf("\n%f %f\n", x, y);
        glVertex3f(x, y, z);
    }
    
    glEnd();

}

void display()
{
    glClearColor( 0.95, 0.95, 0.95, 1 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glEnable(GL_SCISSOR_TEST);
    
    glViewport(0, 0, edgeLength, edgeLength);
    glScissor(0, 0, edgeLength, edgeLength);
    display_obj();
    
    glViewport(edgeLength, 0, 300, .5*edgeLength);
    glScissor(edgeLength, 0*edgeLength, 300, .5*edgeLength);
    display_yt();
    
    glViewport(edgeLength, 0.5*edgeLength, 300, .5*edgeLength);
    glScissor(edgeLength, 0.5*edgeLength, 300, .5*edgeLength);
    display_xt();
    
    glDisable(GL_SCISSOR_TEST);
    
    glutSwapBuffers();
}


void keyboard(unsigned char c, int x, int y)
{
    if (c == 32)
    {
        steps = (++steps) % 8;
        if (pointCount == 4 && steps)
        {
            timeSteps = 0;
            glutTimerFunc(100, timer, 0);
        }
    }
    
    glutPostRedisplay();
}

void mouse(int button, int state, int x, int y)
{
    int viewport[4] = {0, 0, edgeLength, edgeLength};
    double modelview[16];
    double projection[16];
    float winX, winY, winZ;
    double posX, posY, posZ;
    
    glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
    glGetDoublev( GL_PROJECTION_MATRIX, projection );
    //glGetIntegerv( GL_VIEWPORT, viewport );
    
    if (button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
        
        gluUnProject( (float)(x), (float)(600-y), 0.5, modelview, projection, viewport, &posX, &posY, &posZ);
        printf("%d %d, %f %f %f \n", x, y, posX, posY, posZ);
        
        if (posX <= left+1 || posX >= right-1 || posY <= bottom+1 || posY >= top-1)
            return;
        
        pointCount = (++pointCount)%5;
        
        switch (pointCount) {
            case 4:
                R4[0] = posX;
                R4[1] = posY;
                R4[2] = posZ;
                
                timeSteps = 0;
                steps = 0;
                
                break;
            case 2:
                R1[0] = posX;
                R1[1] = posY;
                R1[2] = posZ;
                break;
            case 3:
                P4[0] = posX;
                P4[1] = posY;
                P4[2] = posZ;
                break;
            case 1:
                P1[0] = posX;
                P1[1] = posY;
                P1[2] = posZ;
                break;
            default:
                break;
        }
        
        glutPostRedisplay();
    }
}

int main(int argc, char** argv)
{
    glutInit( &argc, argv );		// Initialize GLUT
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
    glutInitWindowSize( edgeLength + 300, edgeLength );
    glutCreateWindow( "Hermite Curve Demo" );
    
    glutDisplayFunc( display );	// Setup GLUT callbacks
    
    glutKeyboardFunc(keyboard);
    glutMouseFunc(mouse);
    
    glutMainLoop();
    
    return 0;
}
