//
//  main.cpp
//  BSP
//
//  Created by Kalpesh Padia on 10/7/15.
//  Copyright © 2015 Kalpesh Padia. All rights reserved.
//

#include <iostream>
#include <string>

using namespace std;

string tree[] = {"3", "2", "4", "5a", "1", "", "5b"};
int viewPoint[] = {1, 0, 0, -1, -1, -1, -1, -1};


void recurseParseTree(int root, int count, int maxCount)
{
    if (tree[root] == "")
        return;
    if (viewPoint[root] == 1)
    {
        if (count < maxCount)
        {
            recurseParseTree(2*root + 2, count + 1, maxCount);
            cout << " " << tree [root];
            if (count + 1 < maxCount)
                recurseParseTree(2*root + 1, count + 2, maxCount);
            else
                cout << " F";
        }
        else
            cout << " B "<<tree[root] << " F";
    }
    else if (viewPoint[root] == 0)
    {
        if (count < maxCount)
        {
            recurseParseTree(2*root + 1, count + 1, maxCount);
            cout << " " << tree [root];
            if (count + 1 < maxCount)
                recurseParseTree(2*root + 2, count + 2, maxCount);
            else
                cout << " B";
        }
        else
            cout << " F "<<tree[root] << " B";
    }
    else
        cout<< " " <<tree[root];
}

int main(int argc, const char * argv[]) {
    // insert code here...
    for (int i=0; i<5; i++)
    {
        recurseParseTree(0, 0, i);
        //cout << endl;
        cin.get();
    }
    
    return 0;
}
